#!/bin/sh

echo "Waiting for credentials file to exist..."

# while ! nc -z db 9051; do
#   sleep 1
# done

echo "Credentials found. Starting transports."

if [ $ENABLE_TOR = 'TRUE' ]; then
	echo "Starting Tor Backend."
	tor &
fi

exec "$@"
