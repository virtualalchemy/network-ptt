import base64
import json
import logging
import os
import signal
import sys
import threading
import time

# from transport.network.mqtt import mqttwrapper
import traceback

from app.db import create_tables
# from app.globals import dbwrap, mqttwrap
from app.globals import dbwrap, mqttc, selfkeys, mesh, pendingradiomessages
from app.network.callbacks import get_messages, post_message
from app.network.interface import meshtasticinterface, networkinterface
from app.network.rpc_server import messagesendworker
import paho.mqtt.client as mqtt

# from transport.network.onionnetworking import briar, BriarKey, torcontroller
from app.network.onionnetworking import BriarKey, briar
from app.network.packets import base64tobytes, bytestobase64, datapacket, radiopacket
from app.network.rpc_server import instantiate_zerorpc_server

from app.libs.keyhandler import readkeys

if os.getenv('ENABLE_TOR') == 'TRUE':
    from app.globals import torwrap

divider = "-" * 100

listeningport = int(os.getenv("TOR_MESSAGEPORT"))
print("detected mqtt server:", os.getenv("MQTTSERVER"))
# mqtt = mqttwrapper(os.getenv('MQTTSERVER'), 1883)

TOPICS = [os.getenv('INCOMINGTOPIC')]

tc = None
continueflag = True

def on_connect(client, userdata, flags, reason_code, properties) -> None:
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    for i in TOPICS:
        print('subscibing to:',i)
        client.subscribe(i)

def on_message(client, userdata, msg) -> None:
    """
    This function handles the received messages from a frontend and will be called
    every time a message is received via MQTT
    """
    print("received a message via mqtt:", msg.topic, msg.payload)

def publishmessage():
    mqtt_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id='backend', protocol=mqtt.MQTTv5)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    topic = os.environ['INCOMINGTOPIC']
    topic2='incoming'
    print(topic)
    print(type(topic))
    print('connecting to mqtt broker:', topic, os.environ['MQTTPORT'])
    mqtt_client.loop_start()
    mqtt_client.connect(os.environ['MQTTSERVER'], int(os.environ['MQTTPORT']), 60)
    print('sending in 10s')
    time.sleep(10)
    print('sending message to',topic, 'BACKEND')
    while True:
        succ = mqtt_client.publish(topic=topic, payload='BACKEND')
        # print('success:',succ)
        time.sleep(10)

def construct_ack(radioid: int, radiokey: str, radiopacket_obj: radiopacket) -> radiopacket:
    if type(radioid) != type(1):
        raise TypeError('Radioid should be an int')
    if type(radiokey) != type(''):
        raise TypeError('Radiokey should be base64 string')
    dpack = datapacket(
        1, 
        radiopacket_obj.dp.dest, 
        radiopacket_obj.dp.source,
        radiopacket_obj.hash()
        )
    print('constructed datapacket:', dpack)
    ackpack = radiopacket(
        radioid, 
        radiokey, 
        dpack
    )
    return ackpack

def signalreceived_radio(packet):
    conts = dbwrap.getcontacts()
    dpack = None
    cont = None
    pack = None

    for c in conts:
        # print('rid:', base64tobytes(c['radioid'])[0], 'p0:', packet[0])
        if c['radioid'] == packet[0]:
            cont = c
            pack = radiopacket.from_bytes(packet, c['radiokey'])

    if pack != -1 and pack != None:
        '''
        the received packet corresponds to a contact in the contacts 
        and is valid. send an ACK response and continue with the business logic.
        '''
        '''
        get the hash of the packet. if it exists in the pendingradiomessages
        dict, it's an ACK from a previously sent message.
        '''
        print('-'*100)
        print('received packet:', pack)
        print('base64 of payload:', bytestobase64(pack.dp.message))
        print('pendingradiomessages:', pendingradiomessages)
        # h = pack.hash()
        # h = bytestobase64(pack.hash())
        h = bytestobase64(pack.dp.message)
        if h in pendingradiomessages:
            print('hash is in pendingradiomessages:', h)
            d = pendingradiomessages[h]['pack'].dp.todict()
            id = dbwrap.insertmessage(
                mestype=d["type"],
                source=bytestobase64(d["source"]),
                dest=bytestobase64(d["dest"]),
                payload=d["payload"]
            )
            payload = {"id": id, "source": bytestobase64(d['source']), "dest": bytestobase64(d['dest']), "type": d['type']}
            print('signalling frontend:', json.dumps(payload))
            mqttc.publish(os.environ['OUTGOINGTOPIC'], payload=json.dumps(payload))
            del pendingradiomessages[h]
        else:
            # it's not an ACK, so construct an ACK packet and send it.
            dpack = datapacket(1, pack.dp.dest, pack.dp.source, h)
            print('constructed datapacket:', dpack)
            # ackpack = radiopacket(
            #     base64tobytes(cont['radioid']), 
            #     base64tobytes(cont['radiokey']), 
            #     dpack
            #     )
            # mesh.send_raw(ackpack)
            mesh.send_raw(construct_ack(cont['radioid'], cont['radiokey'], pack))
            signalreceived(pack.dp.getcontents())
    else:
        logging.critical('poorly formatted radio packet received. dropping:')
        logging.critical(packet)

def signalreceived(packet):
    global dbwrap
    print("received packet:", packet)
    print("inserting into db...")
    try:
        dp = datapacket.parsedatapacket(packet)
        print("parsed packet:")
        dpdict = dp.todict()
        print(dpdict)
        id = dbwrap.insertmessage(
            dp.type, 
            bytestobase64(dp.source), 
            bytestobase64(dp.dest), 
            dp.message
            )
        print("inserted object id:", id)
        # mqttwrap.publish_message_frontend(id, bytestobase64(dp.source), dp.type)

        payload = {"id": id, "source": bytestobase64(dpdict['source']), "dest": bytestobase64(dpdict['dest']), "type": dpdict['type']}
        print('publishing mqtt message:',os.getenv("INCOMINGTOPIC"),json.dumps(payload))
        success = mqttc.publish(topic=os.getenv("INCOMINGTOPIC"), payload=json.dumps(payload))
        # help(success)
        print('publishing success:',success)

    except Exception as e:
        print("Malformed packet or bad connection.")
        print(e)
        print(traceback.format_exc())
    return


def main():
    zerorpc_server_thread = threading.Thread(target=instantiate_zerorpc_server)
    zerorpc_server_thread.start()

    create_tables()

    dbwrap.importcontact('self', bytestobase64(selfkeys.public_transport()), bytestobase64(selfkeys.public_signing()))

    # set up a server listener
    net = networkinterface(listeningport, signalreceived)
    net.start()

    # start the mesh radio interface
    mesh.registercallback(signalreceived_radio)
    mesh.start()

    # map all the ephemeral ports to all the contacts in the database
    if os.getenv('ENABLE_TOR') == 'TRUE':
        print('Tor enabled. Mapping ephemeral ports.')
        conts = dbwrap.getcontacts()
        for i in conts:
            print(divider)
            print(i)
            username = i["username"]
            inkey = i["inkey"]
            if inkey != None:
                print("mapping for key:", username, "inkey:", inkey)
                torwrap.changeportmap({listeningport: listeningport}, inkey)
    else:
        print('TOR NOT ENABLED. Skipping mapping contact receive sockets')

    while continueflag:
        time.sleep(1)

    sys.exit(0)

    #     time.sleep(5)
    # mes = get_messages()
    # for i in mes:
    #     # mesh.senddata(
    #     #     i['type'],
    #     #     i['dest']['signing_key'],
    #     #     i['payload']
    #     # )
    #     pack = datapacket(
    #         i['type'],
    #         i['source']['signing_key'],
    #         i['dest']['signing_key'],
    #         i['payload']
    #     )
    #     net.senddata(
    #         i['dest']['oniprinton_address'],
    #         listeningport,
    #         pack.getcontents(),
    #         tor=True
    #     )


def testfunc():
    print("starting main test loop...")
    while 1:
        time.sleep(1)


def ctrlc_handler(sig, frame):
    print("ctrlc pressed. attempting to exit gracefully...")
    continueflag = False


if __name__ == "__main__":
    signal.signal(signal.SIGINT, ctrlc_handler)
    main()
    # publishmessage()
