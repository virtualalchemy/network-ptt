import json
import os

from app.network.onionnetworking import BriarKey


def write_briar_key(bkey: BriarKey) -> None:
    with open(os.environ["KEYSFILE"], "w+") as f:
        f.write(json.dumps(json.loads(bkey.serialize()), indent=4))


def writekeys(filename: str, username: str, seed: str) -> None:
    """
    generates a briarkey, serializes it, and stores it in the specified filename

    Parameters
    ----------
    filename : str
            location of the file to store the keys in

    username : str
            username for the given keys

    seed : str
            seed to use to generate the keys

    Returns
    ----------
    None
    """
    print("writing keys to:", filename, "username:", username, "seed:", seed)
    bkey = BriarKey(seed, username=username)
    write_briar_key(filename, bkey)


def readkeys(filename: str) -> BriarKey:
    """
    reads the keys located at filename and returns them as a briarkey object

    Parameters
    ----------
    filename : str
            filename for the keys file

    Returns
    ----------
    BriarKey
            key stored in the file
    """
    try:
        with open(filename, "r") as f:
            return BriarKey.loadserialized(f.read())
    except Exception as e:
        return None


def testwritekeys():
    fname = "keystest.json"
    username = "alice"
    seed = "rwgoie"
    # seed = b'i\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9a'
    bkey = BriarKey(seed)
    print("Generated key:")
    print(bkey)
    writekeys(fname, username, seed)


def writetestkeys():
    fname = "keysfile.json"
    username = "alice"
    seed = "a" * 32
    # seed = b'i\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9ai\xa6\x9a'
    bkey = BriarKey(seed, username=username)
    print("Generated key:")
    print(bkey)
    writekeys(fname, username, seed)


def testreadkeys():
    fname = "keystest.json"
    k = readkeys(fname)
    print("loaded key:")
    print(k)


if __name__ == "__main__":
    # testwritekeys()
    # testreadkeys()
    # print(readkeys('keysfile.json'))
    writetestkeys()
