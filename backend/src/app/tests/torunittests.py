import queue
import threading
import time
from datetime import datetime as dt

from app.network.onionnetworking import BriarKey, briar, torcontroller

divider = "-" * 100


def testmaxconnections(serverpass, mappingport=13370, externalport=13370):
    tc = torcontroller(serverpass)
    outqueue = queue.Queue()
    numaddys = 500
    threads = []
    stime = dt.now()
    for i in range(numaddys):
        key = BriarKey()
        nkey = BriarKey()
        p = key.private_transport()
        # print('private:',p)
        b = nkey.public_transport()
        # print('public:',b)
        a, b, oa, ob = briar.gettorkeys(
            key.private_transport(), nkey.public_transport()
        )
        # o = tc.maptorsocket([externalport, mappingport], b, False)
        x = threading.Thread(
            target=lambda q: q.put(
                tc.maptorsocket([externalport, mappingport], b, False)
            ),
            args=(outqueue,),
        )
        print("starting thread:", i)
        x.start()
        threads.append(x)
    for i, j in enumerate(threads):
        print("joining thread:", i)
        j.join()
    etime = dt.now()
    while outqueue.qsize() > 0:
        print("service created:", outqueue.get().service_id)
    print(divider)
    print("time started:", stime)
    print("time ended:", etime)
    print("time taken:", etime - stime)


def testmaxconnections2(serverpass):
    tc = torcontroller(serverpass)
    numaddys = 10
    keys = []
    for i in range(numaddys):
        key = BriarKey()
        nkey = BriarKey()
        p = key.private_transport()
        # print('private:',p)
        b = nkey.public_transport()
        # print('public:',b)
        a, b, oa, ob = briar.gettorkeys(
            key.private_transport(), nkey.public_transport()
        )
        keys.append(a)
    ts = dt.now()
    k = tc.maptorsockets([13370, 13370], keys)
    te = dt.now()
    print(divider)
    for i in k:
        print("service:", i, ":", k[i])
    print("time taken:", te - ts)


if __name__ == "__main__":
    serverpass = "apples123"
    testmaxconnections2(serverpass)
