import argparse

import paho.mqtt.client as mqtt

from app.network.onionnetworking import BriarKey
from app.network.packets import datapacket, generalizedpacket


def connectfunc(host, port, topic, message):
    print("Sending to", host, ":", port, "on topic:", topic)
    print(message)
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    mqttc.connect(host, port, 60)
    succ = mqttc.publish(topic, message)
    print("message success:", succ)
    mqttc.disconnect()


def getkeystestdata():
    contactnames = ["bob", "jeff", "steve", "fred", "james"]
    contactkeys = ["b" * 32, "c" * 32, "d" * 32, "e" * 32, "f" * 32]
    keys = []
    for i, j in enumerate(contactnames):
        keys.append(BriarKey(contactkeys[i], username=j))
    return contactnames, keys


if __name__ == "__main__":
    ap = argparse.ArgumentParser("transmit a given message over mqtt")
    ap.add_argument("host", help="host to connect to")
    ap.add_argument("port", help="port to connect to", type=int)
    ap.add_argument("topic", help="topic to transmit on")
    ap.add_argument("message", help="string contents of the message")
    ap.add_argument("-td", "--testdata", help="transmit test packet")
    args = ap.parse_args()

    if not args.testdata:
        connectfunc(args.host, args.port, args.topic, args.message)
    else:
        akey = BriarKey("a" * 32, username="alice")
        bkey = BriarKey("b" * 32, username="bob")
        mes = None
        if args.testdata == "message":
            mes = datapacket(
                1, akey.public_transport(), bkey.public_transport(), b"hi bob"
            )
            print("created packet:", mes.getcontents())
            connectfunc(args.host, args.port, args.topic, mes.getcontents())
        elif args.testdata == "contact":
            names, keys = getkeystestdata()
            for i, j in enumerate(keys):
                mes = generalizedpacket(
                    {
                        "type": "create_contact",
                        "username": names[i],
                        "pubkey_transport": j.public_transport(),
                        "pubkey_signing": j.public_signing(),
                    }
                )
                print("created packet:", mes.getcontents())
                connectfunc(args.host, args.port, args.topic, mes.getcontents())
        elif args.testdata == "keys":
            mes = generalizedpacket(
                {"type": "create_keys", "username": "alice", "seed": "a" * 32}
            )
            print("created packet:", mes.getcontents())
            connectfunc(args.host, args.port, args.topic, mes.getcontents())
