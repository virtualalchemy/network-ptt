import argparse
import base64

import paho.mqtt.client as mqtt

from app.network.interface import networkinterface, senddata
from app.network.onionnetworking import BriarKey, briar, tor_connect, torcontroller
from app.network.interface import meshtasticinterface
from app.network.packets import *

# from db.db import *

divider = "-" * 100
ak = BriarKey("alice", username="alice")
bk = BriarKey("bob", username="bob")
ik, ok, ia, oa = briar.gettorkeys(ak.private_transport(), bk.public_transport())
mesh = None

def getkeystestdata():
    contactnames = ["bob", "jeff", "steve", "fred", "james"]
    contactkeys = ["b" * 32, "c" * 32, "d" * 32, "e" * 32, "f" * 32]
    keys = []
    for i, j in enumerate(contactnames):
        keys.append(BriarKey(contactkeys[i], username=j))
    return contactnames, keys


def connectfunc(host, port, topic, message):
    print("Sending to", host, ":", port, "on topic:", topic)
    print(message)
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    mqttc.connect(host, port, 60)
    succ = mqttc.publish(topic, message)
    print("message success:", succ)
    mqttc.disconnect()


def testconnect(address=None):
    port = 13370
    if address == None:
        print("connecting to:", ia + ".onion", "on port", port)
        s = tor_connect(ia + ".onion", port)
    else:
        print("connecting to:", address, "at port:", port)
        s = tor_connect(address, port)
    # print(s.recv(4000))
    dp = datapacket(1, bk.public_transport(), ak.public_transport(), b"hi alice")
    s.send(dp.getcontents())
    s.close()


def servercallback(bytesstring):
    print(bytesstring)


def testserver(server=None):
    print("akey:", ak)
    print("bkey:", bk)
    s = networkinterface(13370, servercallback)
    tc = torcontroller(password="password123", address=server)
    print("setting up server for:")
    print(ik)
    print(ia)
    tc.changeportmap({13370: 13370}, ik)
    s.start()
    a = input("hit enter to exit")
    s.close()


def networkcallback(pack):
    print("packet received:", pack)


def testtorreceive():
    inkey, outkey, inaddr, outaddr = briar.gettorkeys(
        bk.private_transport(), ak.public_transport()
    )
    print("For key 1:", ak)
    print("For key 2:", bk)
    print(
        "inkey:", inkey, "\noutkey:", outkey, "\ninaddr:", inaddr, "\noutaddr", outaddr
    )
    print("creating tor controller")
    tc = torcontroller(password="password123")
    print("starting network interface")
    n = networkinterface(13376, networkcallback)
    n.start()
    print("mapping tor socket for alice -> bob")
    print("bob hash:", bk.exportable())
    o = tc.maptorsocket({13370: 13376}, inkey)
    print(o)
    print("inkey", inkey, "\noutkey:", outkey, "\ninaddr", inaddr, "\noutaddr", outaddr)
    s = networkinterface(13376, servercallback)
    a = input("hit enter to exit")
    s.close()


def testtransmit():
    """
    tests transmit from alice to bob
    """
    akey = BriarKey("a" * 32, username="alice")
    bkey = BriarKey("b" * 32, username="bob")
    print("akey:", akey)
    print("bkey:", bkey)
    print("Sending message to:")
    print(bkey)
    senddata(
        1,
        akey.public_transport(),
        bkey.public_transport(),
        b"hi bob",
        akey.private_transport(),
    )


def testtortransmit():
    inkey, outkey, inaddr, outaddr = briar.gettorkeys(
        bk.private_transport(), ak.public_transport()
    )
    outaddr += ".onion"
    print(divider)
    print("using key:")
    print(bk)
    print(divider)
    print("sending message to:")
    print(ak)
    print(divider)
    print("calculated address:", outaddr)
    dp = datapacket(
        1,
        bytestobase64(bk.public_transport()),
        bytestobase64(ak.public_transport()),
        "hello, alice",
    )
    try:
        s = tor_connect(outaddr, 13370)
        s.send(dp.getcontents())
    except Exception as e:
        print("couldn't connect")
        print(e)


def generatedata():
    akey = BriarKey("a" * 32, username="alice")
    bkey = BriarKey("b" * 32, username="bob")
    print("akey:", akey)
    print("bkey:", bkey)
    print("Sending message to:")
    print(bkey)
    # generate contacts
    names, keys = getkeystestdata()
    for i, j in enumerate(keys):
        mes = generalizedpacket(
            {
                "type": "create_contact",
                "username": names[i],
                "pubkey_transport": j.public_transport(),
                "pubkey_signing": j.public_signing(),
            }
        )
        print("created packet:", mes.getcontents())
        connectfunc("localhost", 17896, "message/control", mes.getcontents())
    # send a message from alice -> bob over mqtt

    mes = datapacket(1, akey.public_transport(), bkey.public_transport(), b"hi bob")
    print("created packet:", mes.getcontents())
    connectfunc("localhost", 17896, "message/outgoing", mes.getcontents())
    # send a message from bob -> alice over tor
    inkey, outkey, inaddr, outaddr = briar.gettorkeys(
        bkey.private_transport(), akey.public_transport()
    )
    print("connecting to:", inaddr, "at port:", 13370)
    s = tor_connect(inaddr + ".onion", 13370)
    # print(s.recv(4000))
    dp = datapacket(1, bkey.public_transport(), akey.public_transport(), b"hi alice")
    s.send(dp.getcontents())
    s.close()
    # send a message from alice -> bob over mqtt
    mes = datapacket(1, akey.public_transport(), bkey.public_transport(), b"hi bob")
    print("created packet:", mes.getcontents())
    connectfunc("localhost", 17896, "message/outgoing", mes.getcontents())


def testtor():
    # tc = torcontroller(password='password123', address='127.0.0.1', port=18098)
    # tc = torcontroller(password='password123', address='127.0.0.1')
    # tc = torcontroller(password='password123', address='127.0.0.1', port=13370)
    tc = torcontroller(password="password123", address="tor", port=9051)
    tc.maptorsocket({13370: 13370}, ik)
    a = input("hit enter to exit")


def testtransmit(address, port, message):
    # s = networkinterface.createsocket("127.0.0.1", 13370)
    # dp = datapacket(1, b"a" * 32, b"b" * 32, b"c" * 20)
    # s.send(dp.getcontents())
    # s.close()
    try:
        s = tor_connect(address, port)
        s.send(message.encode("utf-8"))
        s.close()
    except Exception as e:
        print("couldn't connect")
        print(e)

def testreceivemessage(message):
    pack = datapacket(1, bk.public_transport(), ak.public_transport(), message)
    s = networkinterface.createsocket('127.0.0.1', 13370)
    s.send(pack.getcontents())
    s.close()

def construct_ack(radioid: int, radiokey: str, radiopacket_obj: radiopacket):
    if type(radioid) != type(1):
        raise TypeError("radioid must be an int")
    if type(radiokey) != type(""):
        raise TypeError("radiokey must be a base64 string")
    dpack = datapacket(
        1, 
        radiopacket_obj.dp.dest, 
        radiopacket_obj.dp.source,
        radiopacket_obj.hash()
        )
    print('constructed datapacket:', dpack)
    ackpack = radiopacket(
        radioid, 
        radiokey, 
        dpack
    )
    return ackpack

def messagereceivecallback(message):
    global mesh
    print("message received:", message)
    keys = sorted([bytestobase64(i) for i in [ak.public_transport(), bk.public_transport()]])
    radioid = radiopacket.getnetworkid(keys, ''.join(keys))
    radiokey = radiopacket.getnetworkkey(keys, ''.join(keys) + 'ihatetheantichrist')
    rp = radiopacket.from_bytes(message, radiokey)
    print('parsed radiopacket:', rp)
    print('hash of radiopacket:', rp.hash())
    print('hash of radiopacket (b64):', bytestobase64(rp.hash()))
    ack = construct_ack(radioid,radiokey, rp)
    print('constructed ack:', ack)
    print('base64 of ack:', bytestobase64(ack.dp.message))
    mesh.send_raw(ack)



def testradiotransmit():
    mesh = meshtasticinterface(
        callback=messagereceivecallback, 
        deventry="/dev/ttyACM0"
    )
    mesh.start()
    dpack = datapacket(1, bk.public_transport(), ak.public_transport(), b"hi heather")
    keys = sorted([bytestobase64(i) for i in [ak.public_transport(), bk.public_transport()]])
    radioid = radiopacket.getnetworkid(keys, ''.join(keys))
    radiokey = radiopacket.getnetworkkey(keys, ''.join(keys) + 'ihatetheantichrist')
    rpack = radiopacket(radioid, radiokey, dpack)
    mesh.send_raw(rpack)
    mesh.close()
    
def testradioreceive():
    global mesh
    mesh = meshtasticinterface(
        callback=messagereceivecallback, 
        deventry="/dev/ttyACM1"
    )
    mesh.start()
    
    a = input("hit enter to exit")
    mesh.close()

def mainfunc_general():
    ap = argparse.ArgumentParser("integration tests")
    ap.add_argument("function", help="function to execute")
    ap.add_argument("-ho", "--host", help="function to execute")
    ap.add_argument("-a", "--address", help=".onion address to use")
    ap.add_argument('-m', '--message', help='message to send')
    args = ap.parse_args()

    print("function:", args.function)
    if args.function == "server":
        print("creating server")
        testserver(args.host)
    elif args.function == "client":
        print("creating client")
        testconnect(args.address)
    elif args.function == "map":
        testtor()
    elif args.function == "transmit":
        testtransmit(args.address, 13370, args.message)
    elif args.function == "mqtt":
        testmqtt()
    elif args.function == "tortransmit":
        print("function selected: transmit tor message at address")
        testtortransmit()
    elif args.function == "torreceivemessage":
        print("function selected: receive tor message")
        testtorreceive()
    elif args.function == "tortransmitmessage":
        print("function selected:", "transmit tor message")
        testtortransmit()
    elif args.function == "radiotransmitmessage":
        print("function selected:", "transmit radio message")
        testradiotransmit()
    elif args.function == "radioreceivemessage":
        print("function selected:", "receive radio message")
        testradioreceive()
    elif args.function == "testdata":
        generatedata()
    elif args.function == 'message':
        testreceivemessage(args.message)
    elif args.function == 'fereceive':
        '''
        test receiving a message in the frontend
        '''
        connectfunc('localhost', 17891, 'incoming', 'nigger')
    elif args.function == 'contacthash':
        print(divider)
        print('bob key:')
        print(bk.exportable())
    elif args.function == 'addresses':
        print(divider)
        print('alice key:',ak)
        print(divider)
        print('bob key:',bk)
        print(divider)
        print('alice->bob')
        print('inkey:', ik)
        print('inaddress:',ia)
        print('outkey:',ok)
        print('outaddress:',oa)
    elif args.function == 'radio':
        testradio()
    else:
        print("unknown function:", args.function)



mqttc = None


def on_connect(client, userdata, flags, reason_code, properties):
    client.subscribe("message/incoming")


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))


def testmqtt():
    mqttport = 17891
    print("Creating MQTT wrapper for localhost", ":", mqttport)
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id='integrationtest', protocol=mqtt.MQTTv5)
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.connect("localhost", mqttport, 60)
    mqttc.loop_forever()
    a = input('hit enter to disconnect.')


def mainfunc_db():
    asyncio.run(create_db_and_tables())
    a = input("hit enter to continue")


if __name__ == "__main__":
    # mainfunc_db()
    mainfunc_general()
    # testtortransmit()
    # testtransmit()
    # testmqtt()
    # testtransmit()
    # testconnect()
    # testserver()
    # testtor()
