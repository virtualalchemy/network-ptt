#!/usr/bin/python

# 11.14.11 mjb - initial release
# 05.01.15 wdd - MFM-5512: Added set_SKR() and authenticate() to support Proflex encryption.
# 12.03.15 wdd - MFM-7217: - Changed cryptogram to match keyloader v1.02
# 									- Fixed failure capture and refined reporting for authenticate()
# 									- Added test abort procedures to run_slave_mode() and run_master_mode()
# 									- Added versioning and changed product name

import datetime
import os
import random
import signal
import sys
import time
from multiprocessing import Process, Queue
from threading import Thread

import serial

broadcastaddy = chr(0xFF) + chr(0xFF)
# broadcastaddy_long = chr(0xFF) + chr(0xFF) + chr(0xFF) + chr(0xFF) + chr(0xFF) + chr(0xFF) + chr(0xFF) + chr(0xFF)
broadcastaddy_long = (
    chr(0xDE)
    + chr(0xAD)
    + chr(0xBE)
    + chr(0xEF)
    + chr(0xDE)
    + chr(0xAD)
    + chr(0xBE)
    + chr(0xEF)
)
short_test_address = chr(0x12) + chr(0x34)
panid = chr(0x00) + chr(0x90)
channel = chr(0x0B)
receive_all = chr(0x01)
rf_acks = chr(0x00)
power = chr(0x01)

test_active = True
channel_changed = False
test_channel = 0
verbose = False
divider = "---------------------------------------------"


def printbytestring(bs):
    if type(bs) == type("tes"):
        for i in bs:
            print(hex(ord(i)), end=" ")
    else:
        for i in bs:
            print(hex(i), end=" ")
    print()


class ERM:
    """This class implements support for the ERM (Proflex Radio)"""

    __SOH = b"\x01"
    __EOT = b"\x04"

    lockval = False

    # stream of bytes that should be sent to the erm
    inputstream = None
    # stream of bytes that should be received from the erm
    outputstream = None
    # whether the radio is initialized
    initialized = False

    def initmodule(self):
        self.reset_module()
        self.authenticate()
        self.init_rf_settings(
            panid,
            broadcastaddy_long,
            short_test_address,
            channel,
            receive_all,
            rf_acks,
            power,
        )
        assert self.enter_bypass(), "bypass mode failed"
        self.initialized = True

    def __init__(self, comport, outputs):
        """
        Parameters
        ----------
        comport : string
                port to use for the ERM
        outputs : int
                the number of output streams to use
        """
        # create the serial object
        self.port = serial.Serial(comport, baudrate=19200, timeout=1)
        self.timeout = self.port.timeout
        self.outputstreams = [Queue() for i in range(outputs)]
        print(divider)
        print("initializing the erm with the following settings:")
        print("port:", comport)
        print("output streams:", len(self.outputstreams))
        # initialize the input and output queues
        self.inputstream = Queue()
        self.outputstream = Queue()
        # add the input and output streams to the input and output stream arrays
        self.outputstreams.append(self.outputstream)
        # start the reader and writer process
        self.reader = Process(target=self.readerprocess)
        self.writer = Process(target=self.writerprocess)
        self.writer.start()
        self.reader.start()
        # initialize the radio
        self.initmodule()

    def readerprocess(self):
        """
        takes packets from the input queue and writes them to the erm
        """
        while True:
            pack = self.inputstream.get(block=True)
            if verbose:
                print(divider)
                print("packet detected in the input queue:")
                printbytestring(pack)
                print("sending to the radio")
            self.port.write(pack)

    def readpacketfromerm(self):
        """
        blocks until it can read a packet from the erm then returns it.
        *FOR INTERNAL USE ONLY*

        Returns
        -------
        bytes
                next packet from the radio
        """
        c = b""
        while True:
            # print('f1')
            # time.sleep(.7)
            c = self.port.read()
            if len(c) > 0:
                if c == self.__SOH:
                    count = self.port.read()
                    aptype = self.port.read()
                    rs = c + count + aptype
                    rs = rs + self.port.read(ord(count) - 3)
                    cs = self.__checksum(rs[:-2])
                    return rs

    def writerprocess(self):
        """
        takes packets from the erm and writes them to all the output queues
        """
        while True:
            pack = self.readpacketfromerm()
            if verbose:
                print(divider)
                print("received packet from the erm:")
                printbytestring(pack)
                print(
                    "placing in the output queues (len:", len(self.outputstreams), ")"
                )
            for i in self.outputstreams:
                i.put(pack)

    def close(self):
        """
        closes the erm gracefully
        """
        self.reader.terminate()
        self.writer.terminate()
        self.port.close()

    def connectoutputstream(self, oqueue):
        """
        connects an output queue to the erm output
        Parameters
        ----------
        oqueue : multiprocessing.Queue
                queue to connect to the erm output
        """
        self.outputstreams = self.outputstreams.append(oqueue)

    def addoutputstream(self):
        q = Queue()
        self.outputstreams = self.outputstreams + [q]
        print("adding queue. new size:", len(self.outputstreams))
        return q

    def __checksum(self, data):
        """
        calculates the checksum for a string of byte data

        Parameters
        ---------
        data : bytes
                input string

        Returns
        ---------
        int
                the checksum for the data
        """
        cs = 0
        for b in data:
            cs = (cs + b) & 0xFF
        return cs

    def __make_packet(self, packet):
        """
        Constructs a packet around the data <packet>

        Parameters
        ---------
        packet : char string
                data to be converted into a packet

        Returns
        --------
        bytes
                a syntech-formatted string of data
        """
        chrstr = b""
        for i in packet:
            outp = ord(i).to_bytes(1, byteorder="big")
            chrstr = chrstr + outp

        rs = self.__SOH + (len(packet) + 4).to_bytes(1, byteorder="big")
        rs = rs + chrstr
        cs = self.__checksum(rs).to_bytes(1, byteorder="big")
        rs = rs + cs + b"\x04"
        # print('rs:',rs)
        p = rs
        # print('packet constructed:')
        # for i in p: print(hex(i), end=' ')
        # print()
        return p

    def enter_bypass(self, bypass=True):
        """
        Enters or exits bypass mode
        Parameters
        ---------
        bypass : boolean
                True to enter, False to exit

        Returns
        -------
        whether the request was successful
        """
        if verbose:
            print(divider)
            print("Entering bypass:", bypass)
        # FW = self.__make_packet(chr(0x81) + chr(0xAB) + chr(0xCD) + chr(0xEF))
        if bypass:
            FW = self.__make_packet(chr(0x78) + chr(0x01))
        else:
            FW = self.__make_packet(chr(0x78) + chr(0x00))
        (ap, success) = self.send_packet(FW, self.outputstream)
        (ap, success) = self.send_packet(FW, self.outputstream)
        if success:
            if verbose:
                print("bypass status:")
                printbytestring(ap)
            return True
        else:
            return False

    """
	def transmit_packet(self, address, packet):
		transmits a packet.
		| header (1) (0x01) | size (1) | address (8) (255 for broadcast) | data | checksum (1) | endbyte (1) (0x04) |
		Not for general use.

		Parameters
		----------
		address : char[]
			address for the packet to be sent to. must be 8-length character string. 0xFFFFFFFF for broadcast
		packet : char[]
			data to be sent. must be character string

		Returns
		----------
		bool
			whether the packet was sent successfully

		FW = self.__make_packet(address + packet)
		(ap, success) = self.send_packet(FW, self.outputstream)
		print(ap)
		return success
	"""

    def change_channel(self, channel):
        """
        Changes the radio channel on the ERM
        | header (1) (0x01) | size (1) (0x06) | set RF channel byte (1) (0x05) | channel (1) | checksum (1) | endbyte (1) (0x04) |
        Parameters
        -----------
        channel : int
                channel to switch to.

        Returns
        --------
        bool
                whether the channel was changed successfully
        """
        if verbose:
            print(divider)
            print("changing the channel:")
        FW = self.__make_packet(chr(0x05) + chr(channel))
        (ap, success) = self.send_packet(FW, self.outputstream)
        print(ap)
        return success

    def get_channel(self):
        """
        gets the current radio channel
        Returns
        --------
        int
                the current radio channel
        """
        if verbose:
            print(divider)
            print("getting the channel:")
        FW = self.__make_packet(chr(0x06))
        (ap, success) = self.send_packet(FW, self.outputstream)
        print(ap)
        if ap[2] == 0x86 and success:
            return ap[3]
        return -1

    def check_fips(self):
        """
        checks the fips status
        """
        if verbose:
            print(divider)
            print("checking the fips status:")
        FW = self.__make_packet(chr(0x79))
        (ap, success) = self.send_packet(FW, self.outputstream)
        print("fips check return packet:", ap)
        return success

    def reset_module(self):
        """
        resets the radio module.
        Returns
        -------
        Whether the operation was successful or not
        """
        if verbose:
            print(divider)
            print("resetting the module:")
        FW = self.__make_packet(chr(0x18))
        self.send_packet(FW)
        time.sleep(3)
        return True

    def send_packet(self, packet, responsequeue=None):
        """
        Sends a packet to the radio.

        Parameters
        ----------
        packet : char string
                the syntech-formatted char string to send to the radio
        responsequeue : multiprocessing.Queue
                the queue to listen on for responses. if None, doesn't wait for a response

        Returns
        --------
        bytes, bool
                the bytes response from the radio and whether or not the request was successful.
                returns None if no responsequeue specified
        """
        ack_packet = ""
        success = False
        ptype = packet[2]
        self.inputstream.put(packet)
        if responsequeue is not None:
            (ack_packet, success) = self.read_ack_packet(ptype, responsequeue)
            return (ack_packet, success)
        return

    def read_packet(self, responsequeue):
        """
        reads a packet from a response queue

        Parameters
        ---------
        responsequeue : multiprocessing.Queue
                the output queue to listen on

        Returns
        --------
        bytes
                the next packet that appears on the queue
        """
        while True:
            pack = responsequeue.get(block=True)
            if pack[0] == ord(self.__SOH):
                return pack

    def read_ack_packet(self, dptype, responsequeue):
        """
        like read_packet, but calculates what the response to dptype should be and returns that particular response

        Parameters
        -----------
        dptype : int
                the message type to listen to responses for
        responsequeue : multiprocessing.Queue
                the queue to listen on

        Returns
        ----------
        bytes
                the contents of the next packet matching the response type
        """
        while True:
            pak = responsequeue.get(block=True)
            if not pak[0] == ord(self.__SOH):
                if verbose:
                    print("detected malformed packet! continuing.")
                continue
            aptype = pak[2]
            if aptype != (dptype + 0x80):
                if verbose:
                    print("packet is not the correct type! ignoring.")
                continue
            return pak, True

    def init_rf_settings(
        self, panid, longaddy, shortaddy, channel, receiveall, acks, power
    ):
        """
        initializes the erm with the following settings:

        Parameters
        ----------
        panid : char string [2]
                the panid for the radio
        longaddy : char string [8]
                the long address to use. doesn't really matter
        shortaddy : char string [2]
                the short address to use
        channel : char string [1]
                the radio channel to operate on. between 11-25
        receiveall : char string [1]
                whether to receive all messages. set to 0x01
        acks : char string [1]
                ignored. set to 0x00
        power : char string [1]
                the power level to use. use 1-4

        Returns
        --------
        bool
                whether the operation was successful
        """
        if verbose:
            print(divider)
            print("setting rf settings:")
        FW = self.__make_packet(
            chr(0x2A)
            + panid
            + longaddy
            + shortaddy
            + channel
            + receiveall
            + acks
            + power
        )
        (ap, success) = self.send_packet(FW, self.outputstream)
        if ap[2] == 0xAA and success:
            return True
        return False

    def set_panid(self, panid):
        """
        sets the panid of the radio.
        Parameters
        ---------
        panid : char string [2]
                two-byte character string for the panid

        Returns
        --------
        bool
                whether the operation was successful
        """
        FW = self.__make_packet(chr(0x02) + panid)
        (ap, success) = self.send_packet(FW, self.outputstream)
        if ap[2] == 0x82 and success:
            return True
        return False

    def get_panid(self):
        """
        gets the panid

        Returns
        --------
        int[2]
                panid of the radio, first and then second byte
                -1 if unsuccessful
        """
        FW = self.__make_packet(chr(0x03))
        (ap, success) = self.send_packet(FW, self.outputstream)
        if ap[2] == 0x83 and success:
            return [ap[3], ap[4]]
        return -1

    """def read_data_packet(self, receiving=False):
		rs = None
		success = False
		c = self.port.read()
		if len(c) > 0:
			if c == self.__SOH:
				count = self.port.read()
				aptype = self.port.read()
				rs = c + count + aptype
				rs = rs + self.port.read(ord(count)-3)
				cs = self.__checksum(rs[:-2])
				success = (0xA1 == ord(aptype)) and (cs == ord(rs[-2:-1])) \
							and (rs[len(rs)-1] == self.__EOT)
			else:
				print("char = 0x%02X" % ord(c))
		else:
			pass
		return (rs, success)"""

    def set_rf_channel(self, channel):
        """
        sets the rf channel
        Parameters
        ---------
        channel : integer
                channel to be set to. must be in the interval [11, 26]

        Returns
        --------
        bool
                whether the operation was successful or not.
        """
        if verbose:
            print(divider)
            print("attempting set rf channel")
        if channel <= 26 and channel >= 11:
            FW = chr(0x05) + chr(channel)
            FW = self.__make_packet(FW)
            (ap, success) = self.send_packet(FW, self.outputstream)
            # q_channel = self.get_channel()
            # if (q_channel == -1) or (q_channel != (channel)):
            # 	print("set_rf_channel(%d) failed (%d)!" % (channel,q_channel))
            # 	success = False
            if ap[2] == 0x85 and success:
                return True
        else:
            print("channel out of range")
            success = False
        return False

    def extract_short_packet_data(self, data):
        """
        extracts the payload data from a syntech-formatted short radio receive packet

        Parameters
        ----------
        data : bytes
                a syntech-formatted data packet from the erm

        Returns
        -------
        bytes
                the payload data of the packet
        """
        if (len(data)) < 18:
            return ""
        length = data[17]
        return data[17 + 1 : 17 + 1 + length]

    def receive_rf_packet(self, responsestream):
        """
        blocks until the radio receives a short rf response packet, then returns the result
        Parameters
        ----------
        responsestream : multiprocessing.Queue
                the queue object to listen on

        Returns
        --------
        char[]
                bytestring of the return bytes payload
        """
        pack = None
        while True:
            pack = self.read_packet(responsestream)
            if pack[2] == ord(chr(0xAE)):
                break
        return self.extract_short_packet_data(pack)

    def authenticate(self):
        """
        authenticates the radio to allow changing settings

        Returns
        --------
        whether or not the operation was successful
        """
        FW = chr(0x7A)
        if verbose:
            print(divider)
            print("attempting authentication:")
        # send cryptogram
        FW = (
            FW
            + chr(0x6B)
            + chr(0x66)
            + chr(0xB8)
            + chr(0xB3)
            + chr(0xBC)
            + chr(0x7C)
            + chr(0x4B)
            + chr(0x6C)
        )
        FW = (
            FW
            + chr(0x3E)
            + chr(0xCE)
            + chr(0x96)
            + chr(0x69)
            + chr(0x14)
            + chr(0x5A)
            + chr(0xDD)
            + chr(0x1B)
        )
        FW = self.__make_packet(FW)
        (ap, success) = self.send_packet(FW, self.outputstream)

        # parse response and print error if it failed
        if ap[3] != 0:
            success = False
            if ap[3] == chr(0x02):
                print("authentication failed.")
            elif ap[3] == chr(0x08):
                print(" radio is locked out.")
            else:
                print("Failed to authenticate with radio.")
            print("This test cannot be completed.")
        return success

    def send_rf_data_simple(self, msg):
        """
        sends a message using the radio using the broadcast address and doesn't listen for an ack from the radio

        Parameters
        ----------
        msg : char string
                the message payload

        Returns
        --------
        nothing
        """
        if verbose:
            print(divider)
            print("attempting simple radio send.")
        FW = self.__make_packet(
            chr(0x20) + chr(0x00) + broadcastaddy + chr(0x01) + chr(len(msg)) + msg
        )
        self.send_packet(FW)

    def send_rf_data_short(self, address, msg, outputstream):
        """
        sends a message using the radio to an address
        Parameters
        ---------
        address : char string
                the address to use (short format: 2 bytes)
        msg : char string
                the message payload
        outputstream : multiprocessing.Queue
                the output queue to use to listen to the return data

        Returns
        --------
        bool
                whether the send was successful
        """
        if verbose:
            print(divider)
            print("attempting send to short address")
        if len(msg) > 0:
            FW = chr(0x20)
            # Options
            FW = FW + chr(0x00)

            # Dest Txcvr Addr
            # FW = FW + chr(0xFF) + chr(0x01)
            FW = FW + address

            # Packet ID
            FW = FW + chr(0x01)

            # Target/Sender
            # FW = FW + chr(0x11)
            # Address Mode
            # FW = FW + chr(0x00)
            # Number of data bytes
            FW = FW + chr(len(msg))
            # Data
            for b in msg:
                FW = FW + b
            FW = self.__make_packet(FW)
            (ap, success) = self.send_packet(FW, outputstream)
            if success:
                print("send successful")
                return True
            return False
        else:
            print("no data to send")
            success = False
        return success

    def send_rf_data_long(self, address, msg):
        if verbose:
            print(divider)
            print("attempting to send RF packet (long addresss)")
        FW = self.__make_packet(
            chr(0x24) + chr(0x04) + address + chr(0x01) + chr(len(msg)) + msg
        )
        (ap, success) = self.send_packet(FW)
        print(ap)
        if ap[2] == 0xA4 and success:
            return True
        return False


def stringtochrstring(stringinput):
    outp = ""
    for i in stringinput:
        outp += chr(ord(i))
    return outp


def testtransmit():
    e = ERM("/dev/ttyUSB4")
    e.send_rf_data_simple(stringtochrstring("testing"))
    # e.send_rf_data_short(broadcastaddy, stringtochrstring('testing'), e.outputstream)
    a = input("hit enter to exit")
    e.close()
    sys.exit(0)


def testreceive():
    e = ERM("/dev/ttyUSB4", 1)
    q = e.outputstreams[1]
    # e.connectoutputstream(q)
    print(divider)
    print("listening for messages on the radio...")
    print(e.receive_rf_packet(q))
    a = input("hit enter to exit")
    e.close()
    sys.exit()


"""
if __name__ == '__main__':
	#testtransmit()
	testreceive()
"""
