#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Codec2 Looback Test
# Author: Martin Braun
# Copyright: 2014,2019 Free Software Foundation, Inc.
# Description: An example how to use the Codec2 Vocoder
# GNU Radio version: 3.8.2.0

from distutils.version import StrictVersion

if __name__ == "__main__":
    import ctypes
    import sys

    if sys.platform.startswith("linux"):
        try:
            x11 = ctypes.cdll.LoadLibrary("libX11.so")
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

import signal
import sys
from argparse import ArgumentParser

import sip
from gnuradio import audio, blocks, eng_notation, filter, gr, qtgui, vocoder
from gnuradio.eng_arg import eng_float, intx
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from gnuradio.vocoder import codec2
from PyQt5 import Qt


class loopback_codec2(gr.top_block, Qt.QWidget):
    def __init__(self):
        gr.top_block.__init__(self, "Codec2 Looback Test")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Codec2 Looback Test")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme("gnuradio-grc"))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "loopback_codec2")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.scale = scale = 2**13
        self.samp_rate = samp_rate = 48000
        self.ptt = ptt = 0
        self.port_tx = port_tx = 13370
        self.port_rx = port_rx = 13371
        self.play_encoded = play_encoded = True
        self.input_volume = input_volume = 1
        self.hostname = hostname = "127.0.0.1"

        ##################################################
        # Blocks
        ##################################################
        _ptt_push_button = Qt.QPushButton("Enable TX")
        _ptt_push_button = Qt.QPushButton("Enable TX")
        self._ptt_choices = {"Pressed": 1, "Released": 0}
        _ptt_push_button.pressed.connect(
            lambda: self.set_ptt(self._ptt_choices["Pressed"])
        )
        _ptt_push_button.released.connect(
            lambda: self.set_ptt(self._ptt_choices["Released"])
        )
        self.top_grid_layout.addWidget(_ptt_push_button)
        _play_encoded_check_box = Qt.QCheckBox("Encode Audio")
        self._play_encoded_choices = {True: 1, False: 0}
        self._play_encoded_choices_inv = dict(
            (v, k) for k, v in self._play_encoded_choices.items()
        )
        self._play_encoded_callback = lambda i: Qt.QMetaObject.invokeMethod(
            _play_encoded_check_box,
            "setChecked",
            Qt.Q_ARG("bool", self._play_encoded_choices_inv[i]),
        )
        self._play_encoded_callback(self.play_encoded)
        _play_encoded_check_box.stateChanged.connect(
            lambda i: self.set_play_encoded(self._play_encoded_choices[bool(i)])
        )
        self.top_grid_layout.addWidget(_play_encoded_check_box)
        self._input_volume_range = Range(0, 1, 0.1, 1, 200)
        self._input_volume_win = RangeWidget(
            self._input_volume_range,
            self.set_input_volume,
            "Input volume",
            "counter_slider",
            float,
        )
        self.top_grid_layout.addWidget(self._input_volume_win)
        self.vocoder_codec2_encode_sp_0 = vocoder.codec2_encode_sp(codec2.MODE_1200)
        self.vocoder_codec2_decode_ps_0 = vocoder.codec2_decode_ps(codec2.MODE_1200)
        self.rational_resampler_xxx_1 = filter.rational_resampler_fff(
            interpolation=6, decimation=1, taps=None, fractional_bw=0.4
        )
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
            interpolation=1, decimation=6, taps=None, fractional_bw=0.4
        )
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
            1024,  # size
            8000,  # samp_rate
            "Audio Pre-Encoding",  # name
            1,  # number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_0.set_y_label("Amplitude", "")

        self.qtgui_time_sink_x_0_0.enable_tags(True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(
            qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, ""
        )
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(False)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)

        labels = ["", "", "", "", "", "", "", "", "", ""]
        widths = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        colors = [
            "blue",
            "red",
            "green",
            "black",
            "cyan",
            "magenta",
            "yellow",
            "dark red",
            "dark green",
            "dark blue",
        ]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(
            self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget
        )
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_0_win)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
            1024,  # size
            8000,  # samp_rate
            "Audio Post-Encoding",  # name
            1,  # number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label("Amplitude", "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(
            qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, ""
        )
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)

        labels = ["", "", "", "", "", "", "", "", "", ""]
        widths = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        colors = [
            "blue",
            "red",
            "green",
            "black",
            "cyan",
            "magenta",
            "yellow",
            "dark red",
            "dark green",
            "dark blue",
        ]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(
            self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget
        )
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_char * 1, 48)
        self.blocks_unpacked_to_packed_xx_0 = blocks.unpacked_to_packed_bb(
            1, gr.GR_LSB_FIRST
        )
        self.blocks_udp_source_0 = blocks.udp_source(
            gr.sizeof_char * 1, hostname, port_rx, 80, False
        )
        self.blocks_udp_sink_0 = blocks.udp_sink(
            gr.sizeof_char * 1, hostname, port_tx, 80, False
        )
        self.blocks_uchar_to_float_0 = blocks.uchar_to_float()
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_char * 1, 48)
        self.blocks_short_to_float_0 = blocks.short_to_float(1, scale)
        self.blocks_packed_to_unpacked_xx_0 = blocks.packed_to_unpacked_bb(
            1, gr.GR_LSB_FIRST
        )
        self.blocks_multiply_const_vxx_4 = blocks.multiply_const_ff(ptt)
        self.blocks_multiply_const_vxx_3 = blocks.multiply_const_ff(input_volume)
        self.blocks_multiply_const_vxx_2 = blocks.multiply_const_ff(ptt)
        self.blocks_multiply_const_vxx_1 = blocks.multiply_const_ff(
            1.0 if play_encoded else 0.0
        )
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_ff(
            0.0 if play_encoded else 1.0
        )
        self.blocks_float_to_uchar_0 = blocks.float_to_uchar()
        self.blocks_float_to_short_0 = blocks.float_to_short(1, scale)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.audio_source_0 = audio.source(48000, "", True)
        self.audio_sink_0 = audio.sink(48000, "", True)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.audio_source_0, 0), (self.blocks_multiply_const_vxx_4, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.rational_resampler_xxx_1, 0))
        self.connect(
            (self.blocks_float_to_short_0, 0), (self.vocoder_codec2_encode_sp_0, 0)
        )
        self.connect((self.blocks_float_to_uchar_0, 0), (self.blocks_udp_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.blocks_multiply_const_vxx_1, 0), (self.blocks_add_xx_0, 1))
        self.connect(
            (self.blocks_multiply_const_vxx_2, 0), (self.blocks_float_to_uchar_0, 0)
        )
        self.connect(
            (self.blocks_multiply_const_vxx_3, 0), (self.rational_resampler_xxx_0, 0)
        )
        self.connect(
            (self.blocks_multiply_const_vxx_4, 0), (self.blocks_multiply_const_vxx_3, 0)
        )
        self.connect(
            (self.blocks_packed_to_unpacked_xx_0, 0),
            (self.blocks_stream_to_vector_0, 0),
        )
        self.connect(
            (self.blocks_short_to_float_0, 0), (self.blocks_multiply_const_vxx_1, 0)
        )
        self.connect((self.blocks_short_to_float_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect(
            (self.blocks_stream_to_vector_0, 0), (self.vocoder_codec2_decode_ps_0, 0)
        )
        self.connect(
            (self.blocks_uchar_to_float_0, 0), (self.blocks_multiply_const_vxx_2, 0)
        )
        self.connect(
            (self.blocks_udp_source_0, 0), (self.blocks_packed_to_unpacked_xx_0, 0)
        )
        self.connect(
            (self.blocks_unpacked_to_packed_xx_0, 0), (self.blocks_uchar_to_float_0, 0)
        )
        self.connect(
            (self.blocks_vector_to_stream_0, 0),
            (self.blocks_unpacked_to_packed_xx_0, 0),
        )
        self.connect(
            (self.rational_resampler_xxx_0, 0), (self.blocks_float_to_short_0, 0)
        )
        self.connect(
            (self.rational_resampler_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0)
        )
        self.connect(
            (self.rational_resampler_xxx_0, 0), (self.qtgui_time_sink_x_0_0, 0)
        )
        self.connect((self.rational_resampler_xxx_1, 0), (self.audio_sink_0, 0))
        self.connect(
            (self.vocoder_codec2_decode_ps_0, 0), (self.blocks_short_to_float_0, 0)
        )
        self.connect(
            (self.vocoder_codec2_encode_sp_0, 0), (self.blocks_vector_to_stream_0, 0)
        )

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "loopback_codec2")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_scale(self):
        return self.scale

    def set_scale(self, scale):
        self.scale = scale
        self.blocks_float_to_short_0.set_scale(self.scale)
        self.blocks_short_to_float_0.set_scale(self.scale)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_ptt(self):
        return self.ptt

    def set_ptt(self, ptt):
        self.ptt = ptt
        self.blocks_multiply_const_vxx_2.set_k(self.ptt)
        self.blocks_multiply_const_vxx_4.set_k(self.ptt)

    def get_port_tx(self):
        return self.port_tx

    def set_port_tx(self, port_tx):
        self.port_tx = port_tx

    def get_port_rx(self):
        return self.port_rx

    def set_port_rx(self, port_rx):
        self.port_rx = port_rx

    def get_play_encoded(self):
        return self.play_encoded

    def set_play_encoded(self, play_encoded):
        self.play_encoded = play_encoded
        self._play_encoded_callback(self.play_encoded)
        self.blocks_multiply_const_vxx_0.set_k(0.0 if self.play_encoded else 1.0)
        self.blocks_multiply_const_vxx_1.set_k(1.0 if self.play_encoded else 0.0)

    def get_input_volume(self):
        return self.input_volume

    def set_input_volume(self, input_volume):
        self.input_volume = input_volume
        self.blocks_multiply_const_vxx_3.set_k(self.input_volume)

    def get_hostname(self):
        return self.hostname

    def set_hostname(self, hostname):
        self.hostname = hostname


def main(top_block_cls=loopback_codec2, options=None):
    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string("qtgui", "style", "raster")
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()

    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == "__main__":
    main()
