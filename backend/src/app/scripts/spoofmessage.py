import json

import paho.mqtt.publish as publish

# help(publish)
# exit(0)
# publish.single("message/incoming", json.dumps({'address': 'aaaaaaaa', 'payload': 'test'}), hostname="localhost", port=17896)
publish.single(
    "message/incoming",
    json.dumps({"address": "aaaaaaaa", "payload": "test"}),
    hostname="localhost",
    port=17896,
)
