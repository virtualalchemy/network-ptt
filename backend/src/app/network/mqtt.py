import base64
import json
import logging
import os
import traceback

import paho.mqtt.client as mqtt
from app.db import dbwrapper
from app.libs.keyhandler import readkeys, write_briar_key, writekeys
from paho.mqtt.packettypes import PacketTypes
from paho.mqtt.properties import Properties

from app.network.onionnetworking import BriarKey, tor_connect
from app.network.packets import (
    base64tobytes,
    bytestobase64,
    datapacket,
    generalizedpacket,
)

"""
This file contains the mqtt abstraction layer that handles the communication between
the backend and the frontend.

When a message is received:
* mqttwrapper will insert the message contents into the database.
* mqttwrapper will publish a message to the environment variable INCOMINGTOPIC topic
	on the mqtt server.
	* this message will be a json-formatted string in the following form:
		{
			'id': int
			'source': str
			'type': int
		}
	* the frontend will be notified of this if it is subscribed to the same topic on the
		mqtt server. It will then be able to decide how it will handle this information.
When a message is sent:
* the frontend will publish a message to the OUTGOINGTOPIC environment variable on the
	mqtt server.
	* this message will be the output of network.packet.datapacket.getcontents()
* the backend will then take care of handling the appropriate crypto operations,
	routing, etc.
"""

if os.getenv("TOR_MESSAGEPORT") != None:
    listeningport = int(os.getenv("TOR_MESSAGEPORT"))


class mqttwrapper:
    """
    abstracts the mqtt functions of the app into something more stateful and manageable
    """

    topics = None

    def __init__(self, host: str, port: int):
        """
        Parameters
        ----------
        host : str
                url for the mqtt server

        port : int
                port to address the server on
        """
        global dbwrap
        global torwrap
        # print('Creating MQTT wrapper for',host,':',port)
        from globals import dbwrap

        if os.getenv('TOR_ENABLED') == 'TRUE':
            from app.globals import torwrap

        logging.debug("Creating MQTT wrapper for" + host + ":" + str(port))
        self.mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_message = self.on_message

        self.mqttc.connect(host, port, 60)
        self.topics = {
            "outgoing": os.getenv("OUTGOINGTOPIC"),
            "control": os.getenv("CONTROLTOPIC"),
        }
        self.mqttc.loop_start()
        self.db = dbwrapper()

    def close(self):
        """
        close the mqtt abstraction layer
        """
        self.mqttc.loop_stop()
        self.mqttc.disconnect()

    def __del__(self):
        self.close()

    def on_connect(self, client, userdata, flags, reason_code, properties) -> None:
        """
        The callback for when the client receives a CONNACK response from the server.
        """
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        # client.subscribe("$SYS/#")
        for i in self.topics:
            client.subscribe(self.topics[i])

    def handlecontrol(self, pack: generalizedpacket, props=None) -> None:
        """
        handles the logic for when a control message is received from a frontend
        """
        global dbwrap
        global torwrap
        dat = json.loads(pack.getcontents())
        # logging.debug('Received control message: ' + str(pack.getcontents()))
        print("Received control message: " + str(pack.getcontents()))
        # print('type:',dat['type'])
        # match dat['type']:
        if dat["type"] == "create_contact":
            dbwrap.importcontact(
                dat["username"], dat["pubkey_transport"], dat["pubkey_signing"]
            )
            cont = dbwrap.getcontactbypublictransport(dat["pubkey_transport"])
            logging.debug("New contact imported: " + str(cont))
            logging.debug("mapping new tor connection for: " + str(cont.inaddr))
            try:
                torwrap.changeportmap({listeningport: listeningport}, cont.inkey)
            except Exception as e:
                logging.error("WARNING: couldn't map tor port.")
                logging.error(e)
        elif dat["type"] == "create_keys":
            bkey = BriarKey(dat["seed"], username=dat["username"])
            writekeys(os.getenv("KEYSFILE"), dat["username"], dat["seed"])
            self.db.wipe_contacts(bkey)
        elif dat["type"] == "get_contacts":
            props = msg.properties
            if not hasattr(props, "ResponseTopic") or not hasattr(
                props, "CorrelationData"
            ):
                logging.warning("ERROR: get_contacts requested but no reply requested.")
                return
            corr_id = props.CorrelationData
            reply_to = props.ResponseTopic
            props = mqtt.Properties(PacketTypes.PUBLISH)
            props.CorrelationData = corr_id
            conts = [i.todict() for i in self.dbwrapper.getcontacts()]
            payload = json.dumps(conts)
            self.mqttc.publish(reply_to, payload, qos=1, properties=props)
        elif dat["type"] == "api_response":
            if props == None:
                # print('no properties. exiting.')
                return
            # print('api endpoint hit')
            propsjson = props.json()
            com = dat["command"]
            propsresponse = Properties(PacketTypes.PUBLISH)
            # propsresponse.ResponseTopic = props.ResponseTopic
            propsresponse.CorrelationData = props.CorrelationData
            # print('publishing api repsonse to:')
            if com == "get_privkey":
                bkey = readkeys(os.getenv("KEYSFILE"))
                print(
                    {
                        "topic:": props.ResponseTopic,
                        "payload:": bkey.serialize(),
                        "props:": propsresponse.json(),
                    }
                )
                self.mqttc.publish(
                    topic=props.ResponseTopic,
                    payload=bkey.serialize(),
                    qos=1,
                    properties=propsresponse,
                )
        elif dat["type"] == "import_user_key":
            print("importing user keys...")
            bkey = BriarKey.loadserialized(dat["payload"])
            # writekeys(os.getenv('KEYSFILE'), bkey.username, dat['seed'])
            write_briar_key(os.getenv("KEYSFILE"), bkey)
            self.db.wipe_contacts(bkey)

    def handlemessage(self, pack: datapacket) -> None:
        """
        handles the logic for when an outbound communications message is received from the frontend
        """
        dat = pack.todict()
        print("message:")
        print(pack.getcontents())
        # get the outbound tor address corresponding to the public trasport key
        cont = dbwrap.getcontactbypublictransport(bytestobase64(dat["dest"]))
        if cont == -1:
            logging.warn("Error: attempted send to nonexistent contact:", dat)
            return
        # print('attempting to send to address:',address)
        logging.debug("Attempting to send outbound message to: " + str(cont.outaddr))
        # call tor_connect from onionnetworking to get a socket
        try:
            s = tor_connect(cont.outaddr, 13370)
            if s != -1:
                s.send(pack.getcontents())
                s.close()
                # if the message was successfully sent, insert it into the db
                dbwrap.insertmessage(
                    mestype=dat["type"],
                    source=dat["source"],
                    dest=dat["dest"],
                    payload=dat["payload"],
                )
                # self.mqttc.publish()
        except Exception as e:
            # print('could not send:')
            logging.warning("Could not send message:" + str(e))
            print(traceback.format_exc())

            # print(e)

    # The callback for when a PUBLISH message is received from the server.
    # HANDLE RECEIVED MESSAGES FROM A FRONTEND HERE
    def on_message(self, client, userdata, msg) -> None:
        """
        This function handles the received messages from a frontend and will be called
        every time a message is received via MQTT
        """
        logging.debug("Received MQTT message: " + msg.topic + " " + str(msg.payload))
        print(
            "Received MQTT message: ",
            msg.topic + " " + str(msg.payload),
            "props:",
            msg.properties,
        )
        if msg.topic == self.topics["control"]:
            pack = generalizedpacket.parsepacket(msg.payload)
            print("received generalized packet:", pack.getcontents())
            self.handlecontrol(pack, props=msg.properties)
        elif msg.topic == self.topics["outgoing"]:
            dp = datapacket.parsedatapacket(msg.payload)
            # print('received outgoing datapacket:')
            # print(dp.todict())
            self.handlemessage(dp)

    def publish_message_frontend(self, id: int, source: str, type: int) -> bool:
        """
        publish a message notification via mqtt for any listening frontends.

        Parameters
        ----------
        id : int
        source : string
        type : int
        """
        payload = {"id": id, "source": source, "type": type}
        success = self.mqttc.publish(os.getenv("INCOMINGTOPIC"), json.dumps(payload))
        logging.info(
            "published message from source: ",
            source,
            "and type:",
            str(type),
            "and id:",
            str(id),
        )
        # print('published message from',source,'of type',type,'and id',id,'success:',success)
        return success
