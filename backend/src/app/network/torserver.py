from torpy import TorClient

hostname = "tortest238756.onion"  # It's possible use onion hostname here as well


def test_onion_raw():
    hostname = hostname
    with TorClient() as tor:
        # Choose random guard node and create 3-hops circuit
        with tor.create_circuit(3) as circuit:
            # Create tor stream to host
            with circuit.create_stream((hostname, 80)) as stream:
                # Send some data to it
                stream.send(b"GET / HTTP/1.0\r\nHost: %s\r\n\r\n" % hostname.encode())

                recv = recv_all(stream).decode()
                logger.warning("recv: %s", recv)
                assert "StickyNotes" in recv, "wrong data received"


if __name__ == "__main__":
    pass
