import os
import pathlib
import threading
import traceback
import json
import logging

import zerorpc

from app.globals import dbwrap, mqttc, mesh, pendingradiomessages
from app.libs.keyhandler import readkeys, write_briar_key
from app.network.onionnetworking import BriarKey, tor_connect
from app.network.packets import datapacket, radiopacket, base64tobytes, bytestobase64
from threading import Thread
from queue import Queue
from queue import Empty
import gevent
from datetime import datetime as dt

if os.getenv('ENABLE_TOR') == 'TRUE':
    from app.globals import torwrap

def messagesend_tor(address, port, datapack) -> bool:
    '''
    called when it's time to send a message to a given address
    '''
    try:
        s = tor_connect(address, port)
        if s != -1:
            s.send(datapack.getcontents())
            d = datapack.todict()
            s.close()
            # if the message was successfully sent, insert it into the db
            id = dbwrap.insertmessage(
                mestype=d["type"],
                source=bytestobase64(d["source"]),
                dest=bytestobase64(d["dest"]),
                payload=d["payload"],
            )
            payload = {"id": id, "source": bytestobase64(d['source']),  "dest": bytestobase64(d['dest']), "type": d['type']}
            mqttc.publish(os.environ['INCOMINGTOPIC'], payload=json.dumps(payload))
            return True
    except Exception as e:
        # print('could not send:')
        print("Could not send message:" + str(e))
        print(traceback.format_exc())
        return False

def messagesend_radio(netid: int, netkey: str, datapack: datapacket) -> None:
    '''
    send a message over the radio. takes the netid, the network key, and
    a datapacket, constructs a radiopacket, sends over the radio, and inserts
    it into a dictionary to pend on an ACK message from a receiving client. 
    '''
    if type(netid) != type(1):
        raise Exception("netid must be an int")
    if type(netkey) != type(""):
        raise Exception("netkey must be a base64 string")
    try:
        radiopack = radiopacket(netid, netkey, datapack)
        print('sending radio packet via radio:', radiopack)
        mesh.send_raw(radiopack)
        pendingradiomessages[bytestobase64(radiopack.hash())] = {
            'time': dt.now(),
            'retries': 0,
            'pack': radiopack
        }
    except Exception as e:
        print("Could not send message:" + str(e))
        print(traceback.format_exc())
        return False


class messagesendworker(Thread):
    def __init__(self, *args):
        super().__init__()
        self.messagequeue = Queue()

    def addmessage(self, address, port, datapack):
        payload = {
            'address': address,
            'port': port,
            'datapack': datapack
        }
        self.messagequeue.put(payload)

    def run(self):
        self.running = True
        while self.running:
            try:
                # try getting a message on the queue
                # payload = self.messagequeue.get(timeout=1)
                payload = self.messagequeue.get()
                # print('got message to send:', payload)
                # if it doesn't throw an exception, try sending it over tor
                sent = messagesend_tor(
                    payload['address'],
                    payload['port'],
                    payload['datapack']
                )
                # if the contact isn't reachable over tor, send over the radio
                if not sent:
                    logging.info('couldn\'t send over tor. sending over radio instead.')
                    # print('contact:', payload['datapack'])
                    cont = dbwrap.getcontactbypublictransport(
                        bytestobase64(payload['datapack'].dest)
                        )
                    # print('got contact:', cont)
                    messagesend_radio(
                        cont['radioid'],
                        cont['radiokey'],
                        payload['datapack']
                    )
            except Exception as e:
                if str(type(e)) != "<class '_queue.Empty'>":
                    print('exception occurred during sending:',e, type(e))
                    print(traceback.format_exc())
                pass
            # time.sleep(1)

    def stop(self):
        self.running = False

messageworker = messagesendworker()
messageworker.start()
listeningport = int(os.getenv("TOR_MESSAGEPORT"))

class RPCServer:
    def hello(self, name):
        return f"Mah nigga {name}"

    def has_keysfile(self) -> bool:
        return pathlib.Path(os.getenv("KEYSFILE", "keysfile.json")).exists()

    def create_bkey(self, bkey: str) -> bool:
        briar_key = BriarKey.loadserialized(bkey)
        write_briar_key(briar_key)
        dbwrap.wipe_contacts()

    def get_contacts(self) -> list:
        conts = dbwrap.getcontacts()
        print("got contacts:", conts)
        return conts

    def get_keys(self):
        return readkeys(os.getenv("KEYSFILE", "keysfile.json")).serialize()

    def import_contact(self, hash):
        fields = hash.split("|")
        print("fields:", fields)
        dbwrap.importcontact(fields[0], fields[1], fields[2])
        if os.getenv('ENABLE_TOR') == 'TRUE':
            cont = dbwrap.getcontactbypublictransport(fields[1])
            print("mapping for key:", cont['username'], "inkey:", cont['inkey'])
            torwrap.changeportmap({listeningport: listeningport}, cont['inkey'])

    def send_message(self, type, pubkey_dest, message):
        '''
        called when the backend receives a signal from the frontend to send
            a message.
        '''
        # get the keys for the current user
        keys = readkeys(os.getenv("KEYSFILE", "keysfile.json"))
        # construct a datapacket
        pack = datapacket(type, keys.public_transport(), pubkey_dest, message)
        print("sending message:", type, pubkey_dest, message)
        print(pack.getcontents())
        # get the outbound tor address corresponding to the public trasport key
        cont = dbwrap.getcontactbypublictransport(pubkey_dest)
        if cont == -1:
            logging.warn("Error: attempted send to nonexistent contact:", dat)
            return
        messageworker.addmessage(address=cont['outaddr'], port=13370, datapack=pack)

    def get_conversation(self, pubkey_transport):
        return dbwrap.getconversation(pubkey_transport)

    def getcontactbypublictransport(self, pubkey_transport):
        return dbwrap.getcontactbypublictransport(pubkey_transport)

    def deletecontact(self, pubkey_transport):
        dbwrap.deletecontact(pubkey_transport)

    def get_message(self, id):
        print('got message request:',id)
        mes = dbwrap.getmessage(id)[0]
        print('found message:',mes)
        return mes

    def delete_messages(self):
        dbwrap.deletemessages()

def instantiate_zerorpc_server():
    rpc_server = zerorpc.Server(RPCServer())
    rpc_server.bind("tcp://0.0.0.0:{port}".format(port=os.environ["ZERORPC_PORT"]))
    rpc_server.run()
