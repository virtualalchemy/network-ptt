import base64
import copy
import json
import struct
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.ciphers.modes import CBC
from cryptography.hazmat.primitives.ciphers.algorithms import AES256
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey, Ed25519PublicKey
import hashlib
import logging
import crc8
import os

import crc8

lengthsize = 2
import pickle

messagemappings = {
    "audio": 1,
    "text": 2,
    "image": 3,
    "file": 4,
    "server_create": 5,
    "relay_message": 6,
}


def bytestobase64(bytestring: bytes) -> str:
    return base64.b64encode(bytestring).decode("UTF-8")


def base64tobytes(base64str: str) -> bytes:
    return base64.b64decode(base64str.encode("UTF-8"))


def parsepacket(packetbytes):
    pack = packet(packetbytes)
    return pack.getmessage()


class generalizedpacket:
    def __init__(self, dict):
        self.contents = dict

    def getcontents(self):
        outp = copy.copy(self.contents)
        for i in outp:
            if type(outp[i]) == type(b""):
                outp[i] = base64.b64encode(outp[i]).decode("UTF-8")
        return json.dumps(outp)

    @staticmethod
    def isbase64(contents):
        # print(contents)
        # print(base64.b64decode(contents))
        # print(base64.b64encode(base64.b64decode(contents)))
        if type(contents) == type(""):
            try:
                if (
                    base64.b64encode(base64.b64decode(contents)).decode("UTF-8")
                    == contents
                ):
                    # print('detected')
                    return True
            except:
                pass
        return False

    @staticmethod
    def parsepacket(contents):
        outp = json.loads(contents)
        for i in outp:
            if generalizedpacket.isbase64(outp[i]):
                outp[i] = base64.b64decode(outp[i])
        return generalizedpacket(outp)

    def todict(self):
        return self.contents

    def __str__(self):
        return str(self.contents)


class datapacket:
    """
    | type		|	: 1
    | source	|	: 32
    | dest		|	: 32
    | payload	|	: length-71
    """

    def __init__(self, packettype: int, source: bytes, dest: bytes, message: bytes):
        # print('initializing packet:',type,source,dest,message)
        self.type = packettype
        self.source = base64tobytes(source) if type(source) == type("") else source
        self.dest = base64tobytes(dest) if type(dest) == type("") else dest
        self.message = message.encode() if type(message) == type("") else message

    def getcontents(self):
        return self.source + self.dest + self.type.to_bytes(1, "little") + self.message

    def todict(self) -> dict:
        return {
            "source": self.source,
            "dest": self.dest,
            "type": self.type,
            "payload": self.message
        }

    def __str__(self):
        dict = self.todict()
        for i in dict:
            if type(dict[i]) == type(b""):
                if i != "payload" and dict['type'] != 1:
                    dict[i] = bytestobase64(dict[i])
                else:
                    dict[i] = str(dict[i])
        return json.dumps(dict, indent=4)

    def repr_serializable(self) -> str:
        dict = self.todict()
        for i in dict:
            if type(dict[i]) == type(b""):
                dict[i] = base64.b64encode(dict[i]).decode("UTF-8")
        return json.dumps(dict)

    @staticmethod
    def parsedatapacket(messagebytes):
        return datapacket(
            messagebytes[64], messagebytes[0:32], messagebytes[32:64], messagebytes[65:]
        )


class radiopacket():
    """
    | networkid  |   : 1
    | crc        |   : 1
    | nonce      |   : 4
    | datapacket |   : n

    class to wrap a datapacket with some additional fields for transmission over
        an unsecured radio link
    """
    def hash(self) -> bytes:
        # h = hashlib.new('sha256')
        h = hashlib.sha256()
        h.update(self.netid + self.crc + self.nonce + self.dp.getcontents())
        return h.digest()
        # return hash(self.to_bytes())

    def __init__(self, netid: int, netkey: str, datapack: datapacket):
        '''
        constructor for radiopacket

        Parameters
        ----------
        netid : str
            base64 representation of network id
        netkey : str
            base64 representation of network key
        datapack : datapacket
            datapacket
        '''
        self.dp = datapack
        # self.netid = radiopacket.getnetworkid(participating_keys, network_name)
        if type(netid) != type(1):
            raise TypeError('netid must be an integer')
        if type(netkey) != type(''):
            raise TypeError('netkey must be a base64 string')
        self.netid = netid.to_bytes(1, 'little')
        self.crc = radiopacket.getcrc(datapack.getcontents()).to_bytes(1, 'little')
        self.nonce = os.urandom(4)
        # print('calculated crc for:', datapack.getcontents(), self.crc, ord(self.crc))
        # self.netkey = radiopacket.getnetworkkey(participating_keys, network_pw)
        self.netkey = base64tobytes(netkey)

    @staticmethod
    def getnetworkid(participating_keys: [str], network_name: str) -> int:
        '''
        returns the network id for a list of participating keys in base64, plus the name of the network
        '''
        if type(participating_keys[0]) != type(''):
            raise TypeError('participating_keys must be a list of base64 strings')
        if type(network_name) != type(''):
            raise TypeError('network_name must be a base64 string')
        h = radiopacket.sha256(b''.join([base64tobytes(i) for i in participating_keys]) + network_name.encode('utf-8'))
        return radiopacket.getcrc(h)

    @staticmethod
    def getnetworkkey(participating_keys: [str], network_pw: str) -> str:
        '''
        returns the network key for a list of participating keys in base64
        '''
        if type(network_pw) != type(''):
            raise TypeError('network_pw must be a base64 string')
        if type(participating_keys[0]) != type(''):
            raise TypeError('participating_keys must be a list of base64 strings')
        return bytestobase64(radiopacket.sha256(b''.join([base64tobytes(i) for i in participating_keys]) + network_pw.encode('utf-8')))

    @staticmethod
    def sha256(bytestring):
        h = hashlib.new('sha256')
        h.update(bytestring)
        return h.digest()

    @staticmethod
    def getcrc(bytestring) -> int:
        crc = crc8.crc8()
        crc.update(bytestring)
        return crc.digest()[0]

    @staticmethod
    def decryptbytes(keybytes: bytes, contentbytes: bytes) -> bytes:
        if type(keybytes) == type(''):
            keybytes = base64tobytes(keybytes)
        ivbytes = contentbytes[:16]
        contentbytes = contentbytes[16:]
        h = hashlib.new('sha256')
        h.update(keybytes)
        cipher = Cipher(AES256(h.digest()), CBC(ivbytes)).decryptor()
        dt = cipher.update(contentbytes) + cipher.finalize()
        while dt[-1] == 0:
            dt = dt[:-1]
        return dt

    @staticmethod
    def encryptbytes(keybytes: bytes, contentbytes: bytes) -> bytes:
        if type(keybytes) == type(''):
            keybytes = base64tobytes(keybytes)
        h = hashlib.sha256()
        h.update(keybytes)
        iv = os.urandom(16)
        cipher = Cipher(AES256(h.digest()), CBC(iv)).encryptor()
        while len(contentbytes) % len(iv) != 0:
            contentbytes += chr(0x00).encode('UTF-8')
        ct = cipher.update(contentbytes) + cipher.finalize()
        return iv + ct

    def setnonce(self, nonce):
        self.nonce = nonce

    @staticmethod
    def from_bytes(messagebytes: bytes, network_key: str):
        '''
        parses and validates a datapacket from a radio packet.
        requires the network key in order to decrypt the packet.
        '''
        if type(messagebytes) != type(b''):
            raise TypeError('messagebytes must be bytes')
        if type(network_key) != type(''):
            raise TypeError('network_key must be a base64 string')
        decrypted_network = radiopacket.decryptbytes(
            base64tobytes(network_key), 
            messagebytes[1:]
            )
        # print('decrypted:',decrypted_network)
        crcbyte = int(decrypted_network[0])
        # print('found crc:', crcbyte)
        nonce = decrypted_network[1:5]
        calculatedcrc = radiopacket.getcrc(decrypted_network[5:])
        # print('recalculated crc for:',decrypted_network[1:], calculatedcrc)
        if not calculatedcrc == crcbyte:
            logging.critical('Improperly formed byte:', crcbyte, calculatedcrc)
            return -1
        databytes = decrypted_network[5:]
        dp = datapacket.parsedatapacket(databytes)
        rp = radiopacket(messagebytes[0], network_key, dp)
        rp.setnonce(nonce)
        return rp

    def to_bytes(self):
        if type(self.netid) != type(b''):
            raise TypeError('netid must be bytes')
        print(self.crc, self.nonce, self.dp.getcontents())
        return self.netid + radiopacket.encryptbytes(
            self.netkey,
            self.crc + self.nonce + self.dp.getcontents()
        )

    def todict(self):
        return {
            'netid': self.netid,
            'netkey': self.netkey,
            'nonce': self.nonce,
            'datapacket': self.dp
        }
    
    def __str__(self):
        d = self.todict()
        d['netid'] = d['netid'][0]
        # d['netid'] = ' '.join([str(int(i)) for i in d['netid']])
        # d['netkey'] = ' '.join([str(int(i)) for i in d['netkey']])
        d['netkey'] = bytestobase64(d['netkey'])
        # d['nonce'] = ' '.join([str(int(i)) for i in d['nonce']])
        d['nonce'] = bytestobase64(d['nonce'])
        # d['datapacket'] = str(d['datapacket'].todict())
        d['datapacket'] = str(d['datapacket'].todict())
        return json.dumps(d, indent=4)


class packet:
    """
    packet format:
    | header	|	: 1
    | length	|	: 2
    | body		|	: length - 4
        | type		|	: 1
        | source	|	: 32
        | dest		|	: 32
        | payload	|	: length-71
    | crc		|	: 2
    | footer	|	: 1
    """

    contents = None
    overheadbytes = 5
    messagetype = 1

    @staticmethod
    def crc(bytestr):
        hash = crc8.crc8()
        hash.update(bytestr)
        h = hash.hexdigest()
        return bytes.fromhex(h)

    def __init__(self, bytestr):
        if type(bytestr) != type(b"test"):
            self.message = bytes(bytestr)
        else:
            self.message = bytestr

    @staticmethod
    def getcontents(message):
        global lengthsize
        outp = (
            int(1).to_bytes(1, "little")
            + (len(message) + packet.overheadbytes).to_bytes(lengthsize, "little")
            + message
            + packet.crc(message)
            + int(4).to_bytes(1, "little")
        )
        return outp

    @staticmethod
    def getpayload(contents):
        return contents[3:-2]

    @staticmethod
    def getmessagelength(contents):
        global lengthsize
        return int.from_bytes(contents[1:3], "little")

    def getmessage(self):
        return self.message

    @staticmethod
    def checkcrc(contents):
        # return int.from_bytes(self.contents[-2], 'little') == self.crc(self.getmessage())
        # print(self.getmessage())
        # print(int.from_bytes(self.crc(self.getmessage()), 'little'))
        lval = contents[-2]
        rval = int.from_bytes(packet.crc(packet.getpayload(contents)), "little")
        # print('lval:',lval)
        # print('rval:',rval)
        return lval == rval

    @staticmethod
    def isvalid(contents):
        beginch = contents[0] == 1
        endch = contents[-1] == 4
        lench = len(contents) == packet.getmessagelength(contents)
        crcch = packet.checkcrc(contents)
        # print('begin:',beginch)
        # print('end:', endch)
        # print('lench:', lench)
        # print('crcch:',crcch)
        if beginch and endch and lench and crcch:
            return True
        return False

    def printstr(self, divider=10):
        outp = ""
        for i, j in enumerate(self.getcontents(self.message)):
            outp += str(i) + ":" + str(int(j)) + "\t"
            if i != 0 and i % divider == 0:
                outp += "\n"
        return outp

    def __str__(self):
        return str(self.getcontents(self.message)) + "\n" + self.printstr()

    @staticmethod
    def parsepacket(contents):
        if packet.isvalid(contents):
            return packet(packet.getpayload(contents))
        else:
            raise ValueError("Packet is not valid:" + contents.decode())

    @staticmethod
    def parsedatapacket(bytestr):
        p = packet.parsepacket(bytestr)
        return datapacket.parsedatapacket(p.getmessage())


def testdatapacket():
    divider = "-" * 75
    print(divider)
    print("basic test:")
    testb = b"\1testing"
    pack = packet(testb)
    print("packet:\n", pack)
    print(divider)
    dp = datapacket(1, b"a" * 32, b"b" * 32, b"testing")
    print("datapacket:", dp)
    p = packet(dp.getcontents())
    print("packet:\n", p)
    print("resulting datapacket:")
    conts = p.getcontents(dp.getcontents())
    # print('conts:',conts)
    # print(type(conts))
    print(p.parsedatapacket(conts))


def testencryption():
    key = b'a'*32
    text = b'testingtestingtesting'
    etext = radiopacket.encryptbytes(key, text)
    print('text:',text)
    print('encrypted:',etext)
    dtext = radiopacket.decryptbytes(key, etext)
    print('decrypted:',dtext)
    print(len(text), len(dtext))
    print('success?:', text==dtext)


def testradiopacket():
    divider = "-" * 75
    parts = [bytestobase64(b'a'*32), bytestobase64(b'b'*32)]
    netname = 'testnet'
    netpass = 'netpass'

    print(divider)
    netid = radiopacket.getnetworkid(parts, netname)
    netkey = radiopacket.getnetworkkey(parts, netpass)
    print('calculated network key:',netkey)

    print(divider)
    dp = datapacket(1, b"a" * 32, b"b" * 32, b"testing")
    print("datapacket:", dp)
    print(divider)
    print('datapacket bytes:',dp.getcontents())
    print(divider)
    rp = radiopacket(netid, netkey, dp)
    
    rpb = rp.to_bytes()
    print('radiopacket bytes:',rpb)
    print(divider)
    rp2 = radiopacket.from_bytes(rpb, netkey)
    print('from bytes:\n',rp2)
    print(divider)
    print('hash:',rp.hash())
    print('length (hash):', len(rp.hash()))
    print('rp:',rp)
    print('base64:', bytestobase64(rp.hash()))
    print('rp:',rp)
    print('base64(2):', bytestobase64(rp.hash()))
    print(divider)


def testgeneralizedpacket():
    dat = {"type": 1, "address": b"12345", "command": "create"}
    p = generalizedpacket(dat)
    print("packet:", p)
    print("contents:", p.getcontents())
    p2 = generalizedpacket.parsepacket(p.getcontents())
    print("packet after importing:", p2)


def testisbase64():
    test = "MTIzNDU="
    print(generalizedpacket.isbase64(test))


if __name__ == "__main__":
    # pass
    # testdatapacket()
    # testencryption()
    testradiopacket()
    # testgeneralizedpacket()
    # testisbase64()
