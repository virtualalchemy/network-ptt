import base64
import logging
import os

from sqlalchemy import Integer, LargeBinary, String, create_engine, or_, select, text
from sqlalchemy.orm import DeclarativeBase, Mapped, Session, mapped_column

from app.libs.keyhandler import readkeys
from app.network.onionnetworking import briar
from app.network.packets import bytestobase64, base64tobytes, radiopacket


class Base(DeclarativeBase):
    pass


def create_tables():
    engine = create_engine(url=os.getenv("DATABASE_URL"), echo=True)
    Base.metadata.create_all(engine)


class Message(Base):
    __tablename__ = "messages"
    id: Mapped[int] = mapped_column(primary_key=True)
    type: Mapped[int] = mapped_column(Integer)
    # source: Mapped[str] = mapped_column(String(40))
    # dest: Mapped[str] = mapped_column(String(40))
    # payload: Mapped[str] = mapped_column(String(200))
    source: Mapped[str] = mapped_column(String(60))
    dest: Mapped[str] = mapped_column(String(60))
    payload: Mapped[str] = mapped_column(LargeBinary)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self) -> str:
        return f"Message(id={self.id!r}, type={self.type!r}, source={self.source!r}, dest={self.dest!r}, payload={self.payload!r})"  # noqa: E501


class Contact(Base):
    __tablename__ = "contacts"
    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(String(60))
    pubkey_transport: Mapped[str] = mapped_column(String(90))
    pubkey_signing: Mapped[str] = mapped_column(String(90))
    inkey: Mapped[str] = mapped_column(String(90))
    inaddr: Mapped[str] = mapped_column(String(60))
    outkey: Mapped[str] = mapped_column(String(90))
    outaddr: Mapped[str] = mapped_column(String(60))
    radioid: Mapped[int] = mapped_column(Integer)
    radiokey: Mapped[str] = mapped_column(String(40))

    def todict(self):
        return {
            "username": self.username,
            "pubkey_transport": self.pubkey_transport,
            "pubkey_signing": self.pubkey_signing,
            "inkey": self.inkey,
            "inaddr": self.inaddr,
            "outkey": self.outkey,
            "outaddr": self.outaddr,
            "radioid": self.radioid,
            "radiokey": self.radiokey
        }

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self) -> str:
        return f"Message(username={self.username!r}, \
            pubkey_transport={self.pubkey_transport!r}, \
            pubkey_signing={self.pubkey_signing!r}, \
            inkey={self.inkey!r}, \
            inaddr={self.inaddr!r} \
            outkey={self.outkey!r}, \
            outaddr={self.outaddr!r}, \
            radioid={self.radioid}, \
            radiokey={self.radiokey})"


class dbwrapper:

    def __init__(self):
        self.dbengine = create_engine(
            # url=os.getenv('DATABASE_URL', 'postgresql://postgres:pass123@database:5432/postgres'),
            url=os.getenv("DATABASE_URL"),
            echo=True,
        )

    def drop_db_and_tables(self):
        with self.dbengine.begin() as conn:
            Base.metadata.drop_all(self.dbengine)

    def init_tables(self):
        Base.metadata.create_all(self.dbengine)

    def insertmessage(self, mestype: int, source: str, dest: str, payload: bytes):
        # logging.info("insertmessage called:")
        # logging.info(mestype, source, dest, payload)
        if type(mestype) != type(1):
            raise TypeError("mestype must be an int")
        if type(source) != type(""):
            raise TypeError("source must be a base64 string")
        if type(dest) != type(""):
            raise TypeError("dest must be a base64 string")
        if type(payload) != type(b""):
            raise TypeError("payload must be a bytes array")
        # parsedpayload = bytestobase64(payload) if type(payload) == type(b'') else payload
        with Session(self.dbengine) as sesh:
            mes = Message(
                type=mestype, 
                source=source, 
                dest=dest, 
                payload=payload
            )
            sesh.add(mes)
            sesh.commit()
            sesh.refresh(mes)
            return mes.id

    def importcontact(self, username: str, pkey_transport: str, pkey_signing: str):
        if type(username) != type(""):
            raise TypeError("username must be a string")
        if type(pkey_transport) != type(""):
            raise TypeError("pkey_transport must be a base64 string")
        if type(pkey_signing) != type(""):
            raise TypeError("pkey_signing must be a base64 string")
        userkey = readkeys(os.environ["KEYSFILE"])
        if self.getcontactbypublictransport(pkey_transport) != -1:
            print("attempting to add duplicate packet. cancelling.")
            return -1
        with Session(self.dbengine) as sesh:
            inkey, outkey, inaddr, outaddr = briar.gettorkeys(
                userkey.private_transport(), 
                base64tobytes(pkey_transport)
            )
            keys = sorted([pkey_transport, bytestobase64(userkey.public_transport())])
            radioid = radiopacket.getnetworkid(keys, ''.join(keys))
            radiokey = radiopacket.getnetworkkey(keys, ''.join(keys) + 'ihatetheantichrist')
            print("values:")
            keys = [
                "username",
                "pkey_transport_bytes",
                "pkey_signing_bytes",
                "inkey",
                "inaddr",
                "outkey",
                "outaddr",
                "radioid",
                "radiokey"
            ]
            vals = [
                username,
                pkey_transport,
                pkey_signing,
                inkey,
                inaddr,
                outkey,
                outaddr,
                radioid,
                radiokey
            ]
            """
            username bob
            pkey_transport_bytes b'N\xc5\xc9:tz\x0b6*\xa0\xe42\xb1\xf9j\xc0\xe9$\xe0\xcd\xa7{\x02\x95\xdd*\x03q/\x08\xf7$'
            pkey_signing_bytes b'\xa4\xd9\x12\x07\xa2\x80Q\xbe\xdb\x17\xc9\x13\x92\xa1\xb6%\xd2W\xfc\xce;\xe91#\xd7ui\xa7\x06\xbcl\x1b'
            inkey +Hm4+y3bjuiar4iQ4EMCov+Iee7G3pQawg9atgLf51PEbnR2N5uAsA3t4g71Yroct2SJfONQxB0BUGjdFd6xcQ==
            inaddr wJlNVp9zd4pyS8t6vl0UiI4q54GyKd97wKkjzUseXEN3clAZWXh1Xb4Our8VUJOjW4I74F7ijbXoZIX2c36OZw==
            outkey mfgkyhnc5qfxml7bu4ezxe4pbj5vlw2xeal6767o2f7ramt4uoecgaad
            outaddr kiedyfia5jn5bob5sa5tdjdomvfn76iyh7m6svirgnehlexphsottkyd
            """
            mes = Contact(
                username=username,
                pubkey_transport=pkey_transport,
                pubkey_signing=pkey_signing,
                inkey=inkey,
                inaddr=inaddr,
                outkey=outkey,
                outaddr=outaddr,
                radioid=radioid,
                radiokey=radiokey
            )
            sesh.add(mes)
            sesh.commit()
            sesh.refresh(mes)
            return mes.id

    def testconnection(self):
        url = "postgresql://postgres:pass123@database:5432/postgres"

    def getcontactbypublictransport(self, pubkey_transport: str):
        print("searching:", pubkey_transport)
        with Session(self.dbengine) as sesh:
            result = sesh.execute(
                select(Contact).where(Contact.pubkey_transport == pubkey_transport)
            )
            res = list(result.scalars().all())
            if len(res) == 0:
                return -1
            # for i in res:
            #     print(i[0])
            return res[0].as_dict()

    def wipe_contacts(self):
        with Session(self.dbengine) as sesh:
            sesh.query(Contact).delete()
            sesh.commit()

    def getcontacts(self):
        with Session(self.dbengine) as sesh:
            result = sesh.execute(select(Contact)).scalars().all()
            # print("result:", result)
            return [i.as_dict() for i in result] if len(result) > 0 else []

    def getmessages(self, pubkey_transport):
        with Session(self.dbengine) as sesh:
            result = sesh.execute(
                select(Message).where(Message.source == pubkey_transport)
            )
            res = list(result.fetchall())
            if len(res) == 0:
                return -1
            print("detected:", [i[0] for i in res])
            return [i[0] for i in res]

    def getconversation(self, pubkey_transport):
        print('getting convo for:', pubkey_transport)
        with Session(self.dbengine) as sesh:
            result = sesh.execute(
                select(Message).where(
                    or_(
                        Message.source == pubkey_transport,
                        Message.dest == pubkey_transport,
                    )
                )
            )
            res = list(result.scalars().all())
            # print('res:',res)
            if len(res) == 0:
                return -1
            print("detected:", [i for i in res])
            return [i.as_dict() for i in res] if len(res) > 0 else []

    def deletecontact(self, pubkey_transport):
        with Session(self.dbengine) as sesh:
            q = (
                "delete from Contacts where pubkey_transport = '"
                + pubkey_transport
                + "'"
            )
            print("executing:", q)
            res = sesh.execute(text(q))
            sesh.commit()

    def getmessage(self, id):
        with Session(self.dbengine) as sesh:
            result = list(sesh.execute(
                select(Message).where(
                    Message.id == id
                )
            ).scalars().all())
            return [i.as_dict() for i in result] if len(result) > 0 else []

    def deletemessages(self):
        with Session(self.dbengine) as sesh:
            q = "delete from Messages"
            print("executing:", q)
            res = sesh.execute(text(q))
            sesh.commit()