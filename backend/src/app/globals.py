import logging
import os
import time

from app.db import dbwrapper
from app.network.mqtt import mqttwrapper
from app.network.onionnetworking import torcontroller, BriarKey
from app.network.interface import meshtasticinterface
from app.libs.keyhandler import readkeys
import paho.mqtt.client as mqtt

time.sleep(5)
logging.info("Initializing db wrapper.")
dbwrap = dbwrapper()
logging.info("Initializing tor wrapper.")
if os.getenv('ENABLE_TOR') == 'TRUE':
    torwrap = torcontroller(
        password="password123",
        address=os.getenv("TOR_SERVER"),
        port=int(os.getenv("TOR_CONTROLPORT")),
    )

# mqtt stuff
logging.info("Initializing mqtt wrapper.")
# mqttwrap = mqttwrapper(os.getenv("MQTTSERVER"), 1883)

topics = [
    os.environ['INCOMINGTOPIC']
]

selfkeys = readkeys(os.environ['KEYSFILE'])

def on_connect(client, userdata, flags, reason_code, properties) -> None:
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")
    for i in topics:
        client.subscribe(i)

mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
mqttc.on_connect = on_connect
# mqttc.on_message = on_message
host = os.environ['MQTTSERVER']
port = int(os.environ['MQTTPORT'])
print('connecting mqtt to:',host,port)
mqttc.connect(host, port, 60)
mqttc.loop_start()
if os.getenv('RADIO_DEV') != 'NONE':
    mesh = meshtasticinterface(callback=None, deventry=os.getenv('RADIO_DEV'))
else:
    mesh = meshtasticinterface(callback=None)
pendingradiomessages = {}