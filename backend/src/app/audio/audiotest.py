"""
notes
codec2 information:
    https://github.com/gregorias/pycodec2
python audio streams
    https://python-sounddevice.readthedocs.io/en/0.3.15/api/raw-streams.html#sounddevice.RawInputStream

"""

import copy
import struct
import sys
import time

import numpy as np
import pycodec2
import sounddevice
from sounddevice import RawInputStream, RawOutputStream

filesize = 10000

"""
-------------BEGIN GLOBAL VARIABLES----------------
"""
c2 = pycodec2.Codec2(1200)
INT16_BYTE_SIZE = 2
INT8_BYTE_SIZE = 1
PACKET_SIZE = c2.samples_per_frame() * INT16_BYTE_SIZE
# PACKET_SIZE = c2.samples_per_frame() * INT8_BYTE_SIZE
STRUCT_FORMAT = "{}h".format(c2.samples_per_frame())

index = 0


def readfile(filename):
    """
    read a binary file

    Parameters
    ----------
    filename : string
        name of the file

    Returns
    ----------
    b''
        contents of the file
    """
    with open(filename, "rb") as f:
        return f.read()


def encode_codec2(rawdata):
    """
    returns a codec2 encoded bytestring

    Parameters
    ----------
    rawdata
        an 8000hz int16 bytestring containing the audio data

    Returns
    ----------
    b''
        bytestring containing the codec2 data
    """
    output = b""
    input = rawdata
    while True:
        packet = input[:PACKET_SIZE]
        input = input[PACKET_SIZE:]
        # print('pack: (',str(len(packet)),')',packet,)
        if len(packet) != PACKET_SIZE:
            # 	print('done:', str(len(packet)), 'len_size:',str(PACKET_SIZE))
            break
        packet = np.array(struct.unpack(STRUCT_FORMAT, packet), dtype=np.int16)
        encoded = c2.encode(packet)
        # print('encoded packet size:', sys.getsizeof(encoded))
        output += encoded
    return output


def decode_codec2(encodeddata):
    """
    returns a 8000hz sample rate int16 raw audio bytestring from a codec2
        bytestring

    Parameters
    -----------
    encodeddata : b''
        codec2 encoded audio bytestring

    Returns
    -----------
    b''
        raw audio bytestring
    """
    output = b""
    input = encodeddata
    while True:
        packet = input[:6]
        # print('packet:',packet)
        input = input[6:]
        # print('pack: (',str(len(packet)),')',packet,)
        if len(packet) != 6:
            print("done decoding:", str(len(packet)), "len_size:", str(PACKET_SIZE))
            break
        decoded = c2.decode(packet)
        # print(type(decoded), sys.getsizeof(decoded), len(decoded))
        output += struct.pack(STRUCT_FORMAT, *decoded)
    return output


def encode_decode_codec2(rawdata):
    """
    test function. do not use
    """
    output = b""
    input = rawdata
    while True:
        packet = input[:PACKET_SIZE]
        input = input[PACKET_SIZE:]
        # print('pack: (',str(len(packet)),')',packet,)
        if len(packet) != PACKET_SIZE:
            print("done:", str(len(packet)), "len_size:", str(PACKET_SIZE))
            break
        packet = np.array(struct.unpack(STRUCT_FORMAT, packet), dtype=np.int16)
        encoded = c2.encode(packet)
        print("encoded size:", sys.getsizeof(encoded), "len:", len(encoded))
        packet = c2.decode(encoded)
        print("decoded size:", sys.getsizeof(packet))
        output += struct.pack(STRUCT_FORMAT, *packet)
    return output


audio_data = b""


def play_callback(outdata, frames, time, status):
    """
    callback used by play_raw_data. do not use.
    """
    global audio_data
    data = audio_data[: 2 * frames]
    # print(frames, len(data), len(audio_data))
    audio_data = audio_data[2 * frames :]
    if len(data) / frames == 2:
        outdata[:] = data


def play_raw_data():
    """
    plays the raw audio data stored in audio_data and deletes it.
    tested and working. dont fuck with it
    """
    global audio_data
    with RawOutputStream(
        samplerate=8000,
        device=sounddevice.default.device,
        dtype="int16",
        channels=1,
        callback=play_callback,
    ) as stream:
        while stream.active:
            sounddevice.sleep(30)
            time.sleep(1)
            if len(audio_data) == 0:
                stream.close()
            continue
        stream.close()


def testquerydevices():
    """
    queries the available sound devices and prints them.
    """
    print(sounddevice.query_devices())


# indata: buffer, frames: int, time: CData, status: CallbackFlags
def record_callback(indata, frames, time, status):
    """
    callback used by record_raw. do not use.
    """
    global audio_data
    # print(frames)
    print(frames, len(indata), len(audio_data))
    # print(indata)
    idata = indata[:]
    # print(type(idata),idata, end=' ')
    audio_data += idata
    # indata[:] = idata[:2*frames]


def record_raw(timesleep):
    """
    records for timesleep and returns the audio bytestring
    tested and working. dont fuck with it.

    Parameters
    -----------
    timesleep : float
        time, in seconds, to record for

    Returns
    -----------
    b''
        raw-formatted audio
    """
    global audio_data
    with RawInputStream(
        samplerate=8000,
        device=6,
        channels=1,
        blocksize=10,
        callback=record_callback,
        dtype="int16",
    ) as rs:
        rs.start()
        while rs.active:
            sounddevice.sleep(10)
            time.sleep(timesleep)
            rs.close()


filename = "speech_output.raw"
# filename = 'speech.raw'


def viewhex(data, length):
    """
    view hex data

    Parameters
    -----------
    data : iterable
        iterable data object to view

    length : int
        number of elements to view
    """
    outp = ""
    if len(data) < length:
        length = len(data)
    index = 0
    for i in range(length):
        if i % 16 == 0:
            print("\n" + hex(i) + ":\t", end=" ")
        print(hex(data[i]), end=" ")
    print("\n")


def play(filename):
    global audio_data
    # with open(filename, 'rb') as f:
    # 	audio_data = f.read()
    play_raw_data()


def record(filename):
    global audio_data
    record_raw(3)
    # with open(filename, 'w+b') as f:
    # 	f.write(audio_data)


def testencodedecode():
    global audio_data
    truncatelength = 100000
    # speech = readfile('speech_orig_16k.wav')
    audio_data = readfile("./example/speech.raw")[:truncatelength]
    print("rawdata:")
    viewhex(audio_data, 100)
    play_raw_data()
    a = input("hit enter to continue")
    audio_data = readfile("./example/speech.raw")[:truncatelength]
    dat_enc = encode_codec2(audio_data)
    print("encoded data:")
    viewhex(dat_enc, 100)
    encsize = sys.getsizeof(dat_enc)
    dat_dec = decode_codec2(dat_enc)
    print("decoded data:")
    viewhex(dat_dec, 100)
    decsize = sys.getsizeof(dat_dec)
    audio_data = dat_dec
    play_raw_data()
    print("encoded size:", encsize)
    print("decoded size:", decsize)
    print("compression factor:", decsize / encsize)


def testreadplayspeechfile():
    global audio_data
    filedat = readfile("speech.raw")
    audio_data = filedat
    play_raw_data()


def testrecordplayaudio():
    global audio_data
    record_raw(3)
    play_raw_data()


def testrecordencodeplayaudio():
    global audio_data
    viewlength = 100
    record_raw(3)
    audiocopy = copy.copy(audio_data)
    print("raw audio: size:", sys.getsizeof(audiocopy))
    viewhex(audiocopy, viewlength)
    play_raw_data()
    a = input("hit enter to continue")
    encdat = encode_codec2(audiocopy)
    print("encoded: size:", sys.getsizeof(encdat))
    viewhex(encdat, viewlength)
    audiocopy = decode_codec2(encdat)
    print("decoded: size:", sys.getsizeof(audiocopy))
    viewhex(audiocopy, viewlength)
    audio_data = audiocopy
    play_raw_data()


def mainfunc():
    record(filename)
    # viewhex(audio_data, 300)
    # viewhex(readfile('speech.raw'), 300)
    dat_enc = encode_codec2(audio_data)
    print("encoded data:")
    viewhex(dat_enc, 100)
    exit(0)
    dat_dec = decode_codec2(dat_enc)
    print("audio_data raw size:", sys.getsizeof(audio_data))
    print("audio compressed size:", sys.getsizeof(dat_enc))
    print("compression factor:", sys.getsizeof(audio_data) / sys.getsizeof(dat_enc))
    print("PACKET_SIZE:", PACKET_SIZE, c2.samples_per_frame(), INT16_BYTE_SIZE)
    print("raw data:")
    viewhex(audio_data, 100)
    print("encoded data:")
    viewhex(dat_enc, 100)
    print("decoded data:")
    viewhex(dat_dec, 100)
    # audio_data = encode_decode_codec2(audio_data)
    # viewhex(audio_data, 300)
    # play(filename)


# testquerydevices()
# testencodedecode()
# testreadplayspeechfile()
# testrecordplayaudio()
testrecordencodeplayaudio()
