"""
notes
codec2 information:
    https://github.com/gregorias/pycodec2
python audio streams
    https://python-sounddevice.readthedocs.io/en/0.3.15/api/raw-streams.html#sounddevice.RawInputStream

"""

import copy
import struct
import sys
import time
from threading import Thread

import numpy as np
import pycodec2
import sounddevice
from sounddevice import RawInputStream, RawOutputStream

filesize = 10000

"""
-------------BEGIN GLOBAL VARIABLES----------------
"""
c2 = pycodec2.Codec2(1200)
INT16_BYTE_SIZE = 2
INT8_BYTE_SIZE = 1
PACKET_SIZE = c2.samples_per_frame() * INT16_BYTE_SIZE
# PACKET_SIZE = c2.samples_per_frame() * INT8_BYTE_SIZE
STRUCT_FORMAT = "{}h".format(c2.samples_per_frame())

index = 0


class audiolib:
    audio_data = b""
    output_data = b""
    input_data = b""
    record_toggle = False
    recordthread = None

    def __init__(self):
        self.audio_data = b""
        self.record_toggle = False

    def readfile(self, filename):
        """
        read a binary file

        Parameters
        ----------
        filename : string
            name of the file

        Returns
        ----------
        b''
            contents of the file
        """
        with open(filename, "rb") as f:
            return f.read()

    def encode_codec2(self, rawdata):
        """
        returns a codec2 encoded bytestring

        Parameters
        ----------
        rawdata
            an 8000hz int16 bytestring containing the audio data

        Returns
        ----------
        b''
            bytestring containing the codec2 data
        """
        output = b""
        input = rawdata
        while True:
            packet = input[:PACKET_SIZE]
            input = input[PACKET_SIZE:]
            # print('pack: (',str(len(packet)),')',packet,)
            if len(packet) != PACKET_SIZE:
                # 	print('done:', str(len(packet)), 'len_size:',str(PACKET_SIZE))
                break
            packet = np.array(struct.unpack(STRUCT_FORMAT, packet), dtype=np.int16)
            encoded = c2.encode(packet)
            # print('encoded packet size:', sys.getsizeof(encoded))
            output += encoded
        return output

    def decode_codec2(self, encodeddata):
        """
        returns a 8000hz sample rate int16 raw audio bytestring from a codec2
            bytestring

        Parameters
        -----------
        encodeddata : b''
            codec2 encoded audio bytestring

        Returns
        -----------
        b''
            raw audio bytestring
        """
        output = b""
        input = encodeddata
        while True:
            packet = input[:6]
            # print('packet:',packet)
            input = input[6:]
            # print('pack: (',str(len(packet)),')',packet,)
            if len(packet) != 6:
                print("done decoding:", str(len(packet)), "len_size:", str(PACKET_SIZE))
                break
            decoded = c2.decode(packet)
            # print(type(decoded), sys.getsizeof(decoded), len(decoded))
            output += struct.pack(STRUCT_FORMAT, *decoded)
        return output

    def play_callback(self, outdata, frames, time, status):
        """
        callback used by play_raw_data. do not use.
        """
        data = self.output_data[: 2 * frames]
        # print(frames, len(data), len(audio_data))
        self.output_data = self.output_data[2 * frames :]
        if len(data) / frames == 2:
            outdata[:] = data

    def play_raw_data(self):
        """
        plays the raw audio data stored in audio_data and deletes it.
        tested and working. dont mess with it
        """
        with RawOutputStream(
            samplerate=8000,
            device=sounddevice.default.device,
            dtype="int16",
            channels=1,
            callback=self.play_callback,
        ) as stream:
            while stream.active:
                sounddevice.sleep(30)
                time.sleep(1)
                if len(self.output_data) == 0:
                    stream.close()
                continue
            stream.close()

    def play(self, data):
        """
        plays the raw audio data passed in.

        Parameters
        ----------
        data : b''
            bytestring containing the raw audio data
        """
        self.output_data += copy.copy(data)
        self.play_raw_data()

    def testquerydevices(self):
        """
        queries the available sound devices and prints them.
        """
        print(sounddevice.query_devices())

    # indata: buffer, frames: int, time: CData, status: CallbackFlags
    def record_callback(self, indata, frames, time, status):
        """
        callback used by record_raw. do not use.
        """
        # print(frames)
        # print(frames, len(indata), len(self.audio_data))
        # print(indata)
        idata = indata[:]
        # print(type(idata),idata, end=' ')
        self.input_data += idata
        # indata[:] = idata[:2*frames]

    def record_raw(self):
        """
        records for timesleep and returns the audio bytestring
        tested and working. dont fuck with it.

        Parameters
        -----------
        timesleep : float
            time, in seconds, to record for

        Returns
        -----------
        b''
            raw-formatted audio
        """
        with RawInputStream(
            samplerate=8000,
            device=6,
            channels=1,
            blocksize=10,
            callback=self.record_callback,
            dtype="int16",
        ) as rs:
            rs.start()
            while rs.active:
                while self.record_toggle:
                    sounddevice.sleep(1)
                    time.sleep(0.5)
                rs.close()

    def begin_record(self):
        """
        tells the object to begin recording audio from the microphone
        """
        self.record_toggle = True
        self.recordthread = Thread(target=self.record_raw)
        print("recording started")
        self.recordthread.start()

    def end_record(self):
        """
        tells the object to stop recording audio from the microphone
        """
        self.record_toggle = False
        print("recording finished")
        self.recordthread.join()
