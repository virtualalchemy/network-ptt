# Network PTT

## Name

Network PTT

## Description

A decentralized chat application that uses:

- [React](https://react.dev/) for the frontend
- [FastAPI](https://fastapi.tiangolo.com/lo/) for the backend
- [Tor](https://www.torproject.org/) for the routing protocol

Rather than host a centralized server, the app uses the [Rendezvous Protocol](https://en.wikipedia.org/wiki/Rendezvous_protocol) to deterministically calculate a shared onion address using the sender's private key and the recipient's public key, on both ends.

## Installation

The only requirement for running and developing tthe program is Docker and Docker Compose, either on Mac or Linux.

## Development

To start the frontend and backend run `docker compose up --build`.

### Sqlite Database

Because the devices are decentralized, they don't need a robust database like Postgres or MySQL.

For convenience of development, FastAPI is currently configured to create all tables on startup, then delete them on shutdown. But these directives will eventually be removed. The project is not large, and will probably never require schema mutations.

## Roadmap

### Minimum Viable Product

Right now, we are simply aiming to complete the following

1. The frontend using React with simple HTML-based components.
1. The backend, with only the routes necessary to get the frontend working, inserting messages into a Sqlite database.
1. Asynchronous tasks that will handle syncing messages from the database across Tor.
1. The Tor client server.

### Raspberry Pi Integration

After we achieve the minimum viable product, we plan to build a Raspberry Pi with a speaker, microphone, and some buttons for walkie-talkie-like communication using voice messages. With the program running in Docker on the Pi, the device can act as a telephone, where you can only call someone if you exchange public keys.

### Mesh Networking

Lastly, we plan to integrate [mesh networking](https://en.wikipedia.org/wiki/Mesh_networking) (potentially with [Meshtastic](https://meshtastic.org/)) so that devices can communicate with or without internet (as long as the recipient is in the mesh).

### Future

1. Android compatibility?
2. Encryption (PGP)
3. QR code functionality
4. Monero/Ghost wallets?

## Contributing

Fork it and/or request access to submit a PR.

## Authors and acknowledgment

1. VirtualAlchemy

## License

For open source projects, say how it is licensed.

## Project status

Active Development as of 10-31-22
