"""
info on using mqtt in request-response mode:
    https://www.hivemq.com/blog/mqtt5-essentials-part9-request-response-pattern/
example code from here:
    https://github.com/PrimeshShamilka/mqttv5_request_response_pattern
monkey patch fix information:
    https://stackoverflow.com/questions/29320160/exception-gevent-hub-loopexit-loopexitthis-operation-would-block-forever
* must run the folloing command before the app can start:
    sudo apt install libmpv1
"""

import queue
import random
import time
import socket

import flet as ft
import paho.mqtt.client as mqtt
import pydenticon as pyd
import zerorpc
import json
import os
import threading
from decouple import config
from gevent import monkey
from paho.mqtt.packettypes import PacketTypes
from paho.mqtt.properties import Properties
from network.packets import bytestobase64, base64tobytes, datapacket

from network.onionnetworking import BriarKey

monkey.patch_all()

iconsize = 1024
iconsfile = "assets/contact_icons/"
defaulticon = "assets/icon.png"
divider = "-" * 100

pydgen = pyd.Generator(10, 10, foreground=["#000000"], background="#0000ff")
INCOMINGTOPIC = config("INCOMINGTOPIC")
OUTGOINGTOPIC = config("OUTGOINGTOPIC")
CONTROLTOPIC = config("CONTROLTOPIC")
MQHOST = "127.0.0.1"
MQPORT = 1883
ascii_letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
selfkeys = None

current_page = None
addmessagecallback = None


def randomsequence(leng):
    return "".join([random.choice(ascii_letters) for i in range(leng)])


CLIENT_ID = randomsequence(10)
TOPICS = [INCOMINGTOPIC, OUTGOINGTOPIC, CONTROLTOPIC, CLIENT_ID]


def executecommand(mqttc: mqtt, topic: str, command: str):
    """
    posts a message via mqtt on a given topic with given message contents, blocks for a
    response, and returns the result
    """
    correlation = bytes(randomsequence(5), "UTF-8")
    props = Properties(PacketTypes.PUBLISH)
    # props.UserProperty = ("responsetopic", "2")
    props.ResponseTopic = bytes(CLIENT_ID, "UTF-8")
    props.CorrelationData = correlation
    # props.CorrelationData = correlation
    messages = queue.Queue()

    def receivedmessage(client, userdata, msg):
        # print('received api message. putting in queue:',msg)
        messages.put(msg)

    mqttc2 = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
    mqttc2.connect(MQHOST, MQPORT, 60)
    mqttc2.subscribe(CLIENT_ID)
    mqttc2.on_message = receivedmessage
    mqttc2.loop_start()
    # print('publishing message.')
    mqttc2.publish(topic=topic, payload=command, qos=1, properties=props)
    mes = None
    # print('waiting for response')
    while True:
        # print('qs:',messages.qsize())
        if messages.qsize() > 0:
            mes = messages.get()
            print("getting:", mes.properties.CorrelationData, correlation)
            if mes.properties.CorrelationData == correlation:
                break
        else:
            time.sleep(0.5)
    return mes


def getcontacts(rpc_client):
    conts = rpc_client.get_contacts()
    return conts


def hashtoiconname(hashstring):
    outp = ""
    for i in hashstring:
        if i.isalnum():
            outp += i
    return outp


def loadicons(rpc_client):
    conts = getcontacts(rpc_client)
    for i in conts:
        with open(
            iconsfile + hashtoiconname(i["pubkey_transport"]) + ".png", "wb+"
        ) as f:
            # print('contact',i)
            f.write(pydgen.generate(i["pubkey_transport"], iconsize, iconsize))


def getconversation(rpc_client, pubkey_transport):
    mes = rpc_client.get_conversation(pubkey_transport=pubkey_transport)
    return mes


currentcontact = None
messagefield = None


def on_connect(client, userdata, flags, reason_code, properties) -> None:
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    for i in TOPICS:
        print('subscibing to:',i)
        client.subscribe(i)


def getkey_backend(rpc_client):
    return BriarKey.loadserialized(rpc_client.get_keys())

def createsocket(host, port):
        s = socket.socket()
        s.connect((host, port))
        return s

def messagesview(page: ft.Page, rpc_client):
    global messagefield
    global mqtt_client
    global addmessagecallback
    global selfkeys


    def sendmessageclicked(e):
        print("sendmessage clicked:", currentcontact, messagefield.value)
        if currentcontact != bytestobase64(selfkeys.public_transport()):
            rpc_client.send_message(1, currentcontact, messagefield.value)
        else:
            pack = datapacket(1, selfkeys.public_transport(), selfkeys.public_transport(), messagefield.value.encode('UTF-8'))
            s = createsocket('127.0.0.1', 13370)
            s.send(pack.getcontents())
            s.close()

    def populatemessages(currentcontact):
        messages = []
        conv = rpc_client.get_conversation(currentcontact)
        print(conv)
        if conv != -1:
            for i in conv:
                print("got message:", i)
                if i["type"] == 1 and (i['source'] == currentcontact or i['dest'] == currentcontact):
                    messages.append(
                        ft.Container(
                            ft.ListTile(
                                title=ft.Text(i["payload"].decode("UTF-8")),
                            ),
                            border_radius=10,
                            bgcolor=(
                                "#009900" if i["source"] == currentcontact else "#0000ff"
                            ),
                        )
                    )
        return messages

    messagefield = ft.TextField(label="message", expand=True)
    print(divider)
    print("displaying messages for:", currentcontact)
    cont = rpc_client.getcontactbypublictransport(currentcontact)
    print("contact:", cont)

    messages = populatemessages(currentcontact)
    messview = ft.ListView(controls=messages, expand=True)

    def appendmessage(payload):
        if not 'source' in payload or not 'id' in payload:
            print('no source or id in payload:', payload)
            return
        id = payload['id']
        if currentcontact != payload['source'] and currentcontact != payload['dest']:
            print('message not for me:', currentcontact, payload)
            return
        print('querying rpc for message:',id)
        mess = rpc_client.get_message(id)
        print('appending message!:',mess)
        if mess["type"] == 1:
            # messages.append(
            #     ft.Container(
            #         ft.ListTile(
            #             title=ft.Text(mess["payload"].decode("UTF-8")),
            #         ),
            #         border_radius=10,
            #         bgcolor=(
            #             "#009900" if mess["source"] == currentcontact else "#0000ff"
            #         ),
            #     )
            # )
            messages = populatemessages(currentcontact) 
            messview.controls = messages
            page.update()
    addmessagecallback = appendmessage

    outp = ft.Column()
    outp.controls.append(
        ft.AppBar(
            leading=ft.ElevatedButton("home", on_click=lambda _: page.go("/")),
            title=ft.Text(cont["username"]),
            center_title=True,
            bgcolor=ft.colors.SURFACE_VARIANT,
        )
    )

    outp.controls.append(messview)
    outp.controls.append(
        ft.Row(
            controls=[
                messagefield,
                ft.IconButton(
                    icon=ft.icons.PAUSE_CIRCLE_FILLED_ROUNDED,
                    icon_size=20,
                    tooltip="send message",
                    on_click=sendmessageclicked,
                ),
            ]
        )
    )
    return outp


def createkeysview(page: ft.Page, rpc_client):
    namefield = ft.TextField()
    seedfield = ft.TextField()

    def importkeyfunction(e):
        rpc_client.create_bkey(
            BriarKey(
                seedphrase=seedfield.value,
                username=namefield.value,
            ).serialize()
        )

    controls = [
        ft.AppBar(
            leading=ft.ElevatedButton("home", on_click=lambda _: page.go("/")),
            title=ft.Text("Create Key Screen"),
            center_title=True,
            bgcolor=ft.colors.SURFACE_VARIANT,
        ),
        ft.Text("Name"),
        namefield,
        ft.Text("Seed"),
        seedfield,
        ft.TextButton(text="IMPORT", on_click=importkeyfunction),
    ]
    outp = ft.Column()
    for i in controls:
        outp.controls.append(i)
    return outp


def addcontactview(page: ft.Page, rpc_client):
    hashfield = ft.TextField()

    def importkeyfunction(e):
        rpc_client.import_contact(hashfield.value)

    controls = [
        ft.AppBar(
            leading=ft.ElevatedButton("home", on_click=lambda _: page.go("/")),
            title=ft.Text("Import Screen"),
            center_title=True,
            bgcolor=ft.colors.SURFACE_VARIANT,
        ),
        ft.Text("Hash"),
        hashfield,
        ft.TextButton(text="IMPORT", on_click=importkeyfunction),
    ]
    outp = ft.Column()
    for i in controls:
        outp.controls.append(i)
    return outp


def exporthashview(page: ft.Page, rpc_client):
    controls = [
        ft.AppBar(
            leading=ft.ElevatedButton("home", on_click=lambda _: page.go("/")),
            title=ft.Text("Contact Hash"),
            center_title=True,
            bgcolor=ft.colors.SURFACE_VARIANT,
        ),
        ft.Text("Hash"),
        ft.TextField(getkey_backend(rpc_client).exportable(), read_only=True),
    ]
    outp = ft.Column()
    for i in controls:
        outp.controls.append(i)
    return outp


def contactsview(page: ft.Page, rpc_client):

    def importviewtransition(e: ft.RouteChangeEvent):
        print("transitioning to import view")
        page.go("/createkeysscreen")

    def addcontacttransition(e: ft.RouteChangeEvent):
        print("transitioning to add contact view")
        page.go("/addcontact")

    def exporthashtransition(e: ft.RouteChangeEvent):
        print("exporting contact")
        page.go("/exporthash")

    def clearmessages(e: ft.RouteChangeEvent):
        print("clearing messages")
        rpc_client.delete_messages()

    def messageview(e: ft.RouteChangeEvent):
        global currentcontact
        currentcontact = e.control.data
        print(e.control.data)
        page.go("/messages")

    def deletecontact(e: ft.RouteChangeEvent):
        print("deleting:", e.control.data)
        rpc_client.deletecontact(e.control.data)

    pb = ft.PopupMenuButton(
        items=[
            ft.PopupMenuItem(text="Set Privkey", on_click=importviewtransition),
            ft.PopupMenuItem(text="Add Contact", on_click=addcontacttransition),
            ft.PopupMenuItem(text="Export Contact Hash", on_click=exporthashtransition),
            ft.PopupMenuItem(text="Clear Messages", on_click=clearmessages),
        ]
    )

    loadicons(rpc_client)
    conts = getcontacts(rpc_client)
    contactlist = []
    for i in conts:
        contactlist.append(
            ft.ListTile(
                title=ft.Text(i["username"]),
                leading=ft.Image(
                    src=iconsfile + hashtoiconname(i["pubkey_transport"]) + ".png",
                    fit="contain",
                ),
                # leading=ft.Image(src='assets/icon.png', fit='contain'),
                on_click=messageview,
                data=i["pubkey_transport"],
                trailing=ft.PopupMenuButton(
                    items=[
                        ft.PopupMenuItem(
                            text="Delete",
                            on_click=deletecontact,
                            data=i["pubkey_transport"],
                        )
                    ]
                ),
            )
        )
    outp = ft.Column()
    outp.controls.append(
        ft.AppBar(
            title=ft.Text("BEDROCK"),
            bgcolor=ft.colors.SURFACE_VARIANT,
            actions=[pb],
        )
    )
    for i in contactlist:
        outp.controls.append(i)
    return outp


def testmqtt():
    mqtt_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    print('connecting to mqtt broker:', config('MQTTSERVER'), config('MQTTPORT'))
    mqtt_client.connect(config('MQTTSERVER'), int(config('MQTTPORT')), 60)
    mqtt_client.loop_start()
    for i in TOPICS: mqtt_client.subscribe(i)
    # while True:
    #     time.sleep(1)
    mqtt_client.publish(topic=config('INCOMINGTOPIC'), payload='TEST PUBLISH') #os.environ('INCOMINGTOPIC', 'TEST PUBLISH'))
    a = input('hit enter to exit')

def mqttlistener():
    
    mqtt_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    print('connecting to mqtt broker:', config('MQTTSERVER'), config('MQTTPORT'))
    mqtt_client.connect(config('MQTTSERVER'), int(config('MQTTPORT')), 60)
    mqtt_client.loop_start()
    while True:
        time.sleep(1)

def main(page: ft.Page):
    global mqtt_client
    global current_page
    global addmessagecallback
    global selfkeys

    rpc_client = zerorpc.Client()
    rpc_client.connect("tcp://127.0.0.1:4242")

    selfkeys = BriarKey.loadserialized(rpc_client.get_keys())

    global currentcontact
    global messagefield

    page.add(ft.Text(f"Initial route: {page.route}"))

    def route_change(route):
        elems = None
        page.views.clear()
        if not rpc_client.has_keysfile():
            elems = createkeysview(page, rpc_client)
        elif page.route == "/":
            elems = contactsview(page, rpc_client)
        elif page.route == "/messages":
            elems = messagesview(page, rpc_client)
        elif page.route == "/createkeysscreen":
            elems = createkeysview(page, rpc_client)
        elif page.route == "/addcontact":
            elems = addcontactview(page, rpc_client)
        elif page.route == "/exporthash":
            elems = exporthashview(page, rpc_client)
        page.views.append(elems)
        page.update()

    def on_message(client, userdata, msg) -> None:
        """
        This function handles the received messages from a frontend and will be called
        every time a message is received via MQTT
        """

        print("received a message via mqtt:", msg.payload)
        # current_page.update()
        addmessagecallback(json.loads(msg.payload))
        # route_change('/messages')

    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    mqtt_client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    print('connecting to mqtt broker:', config('MQTTSERVER'), config('MQTTPORT'))
    mqtt_client.connect(config('MQTTSERVER'), int(config('MQTTPORT')), 60)
    mqtt_client.loop_start()
    
    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)


def testgetmessage():
    rpc_client = zerorpc.Client()
    rpc_client.connect("tcp://127.0.0.1:4242")
    print(rpc_client.get_message(40))

if __name__ == "__main__":
    ft.app(main)
    # mqttlistener()
    # testmqtt()
    # testgetmessage()
