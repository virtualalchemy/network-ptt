import os
from dataclasses import dataclass
from typing import List
from urllib.parse import urljoin

import paho.mqtt.client as mqtt
import requests

from network.packets import datapacket


@dataclass
class Message:
    content: str
    content_type: str


# API_ENDPOINT = os.getenv("API_ENDPOINT", "http://database:2341")
API_ENDPOINT = os.getenv("DATABASE_URL")
print("DATABASE URL:", API_ENDPOINT)
incoming_topic = os.getenv("INCOMINGTOPIC")
outgoing_topic = os.getenv("OUTGOINGTOPIC")


def post_message(dpacket: datapacket) -> None:
    url = urljoin(API_ENDPOINT, "/messages")
    data = dpacket.todict()
    r = requests.post(url, data)
    r.raise_for_status()


def mark_message_as_sent(message_id: int) -> None:
    url = urljoin(API_ENDPOINT, f"/messages/{message_id}")
    data = {"is_sent": True}
    r = requests.put(url, data)
    r.raise_for_status()


def get_messages() -> List[dict]:
    url = urljoin(API_ENDPOINT, "messages/unsent")
    r = requests.get(url)
    r.raise_for_status()
    return r.json()
