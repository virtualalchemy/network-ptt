import socket
import sys
import threading

try:
    from queue import *
except ImportError:
    from Queue import *

try:
    import SocketServer as socketserver
except ImportError:
    import socketserver

import datetime
import json
import socket
import sys
import time
from multiprocessing import Lock, Process

from audiolib import audiolib
from socketpool.conn import TcpConnector
from socketpool.pool import ConnectionPool


class EchoServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


class portlistener:
    def __init__(self, port, receivercallback):
        """
        initialize the portlistener object

        Parameters
        ----------
        port : int
            port on localhost to listen on

        receivercallback : *args
            function to be called when the server accepts a connection.
            args[0] will be a socket.socket object
        """
        self.continueserving = True
        self.receivercallback = receivercallback
        self.port = port

    def close(self):
        """
        safely closes the server
        """
        self.continueserving = False
        self.server.server_close()
        self.server.shutdown()

    def start(self):
        """
        starts the server in the background
        """
        # Port 0 means to select an arbitrary unused port
        HOST, PORT = "", 13370

        # eh = EchoHandler()
        # eh.registerreceivercallback(self.receivercallback)
        self.server = EchoServer(
            (HOST, self.port), self.receivercallback, bind_and_activate=False
        )
        self.server.allow_reuse_address = True
        self.server.server_bind()
        self.server.server_activate()
        ip, port = self.server.server_address

        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(
            target=self.server.serve_forever, kwargs={"poll_interval": 0.5}
        )
        # Exit the server thread when the main thread terminates
        server_thread.daemon = True
        server_thread.start()

        print(datetime.datetime.now(), ": starting server on", ip, port)


def testcallback(*args):
    # print(datetime.datetime.now(), 'called callback function with data:',str(args))
    # print(args[0])
    # print(args[1])
    # print(args[2])
    print(
        datetime.datetime.now(), "called callback function with data:", args[0].recv(90)
    )


def testportlistener():
    p = portlistener(testcallback)
    p.start()
    a = input("hit enter to exit")
    p.close()


def createsocket(host, port):
    s = socket.socket()
    s.connect((host, port))
    return s


if __name__ == "__main__":
    testportlistener()
