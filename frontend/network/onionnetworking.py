"""
example code obtained from https://stem.torproject.org/tutorials/over_the_river.html
ephemeral hidden service information:
    https://stem.torproject.org/tutorials/over_the_river.html
inspiration obtained from code.briarproject.org/briar/briar-spec/-/blob/master/protocols/BRP.md

shared key code obtained from:
    https://cryptography.io/en/latest/hazmat/primitives/asymmetric/x25519/

RFC spec located here: https://datatracker.ietf.org/doc/html/rfc8032
"""

import base64
import copy
import hashlib
import json
import queue
import socket
import struct
import threading
from datetime import datetime as dt
from hmac import HMAC
from typing import Optional

import requests
import socks
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives._serialization import (
    Encoding,
    KeySerializationEncryption,
    NoEncryption,
    PrivateFormat,
    PublicFormat,
)
from cryptography.hazmat.primitives.asymmetric.ed25519 import (
    Ed25519PrivateKey,
    Ed25519PublicKey,
)
from cryptography.hazmat.primitives.asymmetric.x25519 import (
    X25519PrivateKey,
    X25519PublicKey,
)
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from nacl.bindings import (
    crypto_sign_ed25519_pk_to_curve25519,
    crypto_sign_ed25519_sk_to_curve25519,
)
from stem import Signal
from stem.control import Controller
from stem.descriptor.hidden_service import HiddenServiceDescriptorV3

from network.packets import base64tobytes, bytestobase64

divider = "-" * 100


class BriarKey:
    """
    convenience class for working with briar key objects.
    """

    dividerchar = "|"

    def __init__(
        self, seedphrase: Optional[str] = None, pkey: bytes = None, username: str = None
    ):
        """
        creates a new briar key. if no parameters are specifed, it will create
            a new random key.

        Parameters
        ----------
        seedphrase : ''
            arbitrary string of text that will be hashed and turned into a key

        pkey : b''
            32-byte raw bytestring containing a briar key
        """
        pbytes = None
        if seedphrase != None and pkey == None:
            pbytes = bytes(hashlib.sha256(seedphrase.encode()).digest())
        elif pkey != None:
            pbytes = pkey
        if pbytes != None:
            # 			self.key = Ed25519PrivateKey.from_private_bytes(pbytes)
            self.key = X25519PrivateKey.from_private_bytes(pbytes)
            self.signingkey = Ed25519PrivateKey.from_private_bytes(pbytes)
        else:
            # 			self.key = Ed25519PrivateKey.generate()
            self.key = X25519PrivateKey.generate()
            self.signingkey = Ed25519PrivateKey.generate()
        self.username = username if username != None else None

    def public_transport(self) -> bytes:
        """
        Returns the 32-byte public transport key
        --------
        b''
            32-byte raw public key
        """
        return self.key.public_key().public_bytes(
            Encoding.Raw,
            PublicFormat.Raw,
        )

    def private_transport(self) -> bytes:
        """
        Returns the 32-byte private key
        --------
        b''
            32-byte raw private key
        """
        return self.key.private_bytes(Encoding.Raw, PrivateFormat.Raw, NoEncryption())

    def private_signing(self) -> bytes:
        """
        Returns the 32-byte private signing key
        """
        return self.signingkey.private_bytes(
            Encoding.Raw, PrivateFormat.Raw, NoEncryption()
        )

    def public_signing(self) -> bytes:
        """
        returns the 32-byte public signing key

        Returns
        -------
        b''
            32-byte public signing key
        """
        return self.signingkey.public_key().public_bytes(
            Encoding.Raw,
            PublicFormat.Raw,
        )

    def todict(self) -> dict:
        """
        Returns
        -------
        {
            b'',
            b'',
            b'',
            b''
        }
            dictionary object containing the raw public and private keys
        """
        outp = {
            "private_transport": self.private_transport(),
            "public_transport": self.public_transport(),
            "private_signing": self.private_signing(),
            "public_signing": self.public_signing(),
        }
        if self.username:
            outp["username"] = self.username
        return outp

    def __str__(self) -> str:
        # return str(base64.b64encode(self.private())) + ' : ' + str(base64.b64encode(self.public()))
        d = self.todict()
        for i in d:
            if type(d[i]) == type(b""):
                d[i] = bytestobase64(d[i])
        return json.dumps(d, indent=4)

    def sign(self, data):
        return self.signingkey.sign(data)

    def public(self) -> str:
        """
        returns a string representation needed to import the contact
        """
        return (
            self.username
            + self.dividerchar
            + bytestobase64(self.public_transport())
            + self.dividerchar
            + bytestobase64(self.public_signing())
        )

    def serialize(self):
        """
        return a string representation of the object
        """
        dat = self.todict()
        for i in dat:
            if type(dat[i]) == type(b""):
                dat[i] = bytestobase64(dat[i])
        return json.dumps(dat)

    def exportable(self):
        return "|".join(
            [
                self.username,
                bytestobase64(self.public_transport()),
                bytestobase64(self.public_signing()),
            ]
        )

    @staticmethod
    def loadserialized(dat):
        data = json.loads(dat)
        # print('data:', data)
        bkey = (
            BriarKey(pkey=base64tobytes(data["private_transport"]))
            if not "username" in data
            else BriarKey(
                pkey=base64tobytes(data["private_transport"]), username=data["username"]
            )
        )
        # print('bkey:',bkey)
        return bkey


class briar:
    """
    library for performing briar functions.
    The most important one for most purposes is gettorkeys().
    documentation on converting a curve25519 (x25519) to an edwards 25519
        (ed25519) public key here:
        https://words.filippo.io/using-ed25519-keys-for-encryption/
    """

    @staticmethod
    def hashfunc(bstringarr):
        """
        calculated a hash from bstring as defined in the briar rendezvouz spec
        Parameters
        ----------
        bstring : [bytestring]
            input bytestring list

        Returns
        ----------
        b''
        """
        hashstr = b""
        for i in bstringarr:
            hashstr += len(i).to_bytes(4, "big") + i
        return hashlib.blake2b(hashstr).digest()

    @staticmethod
    def macfunc(k, bstringarr):
        hashstr = b""
        for i in bstringarr:
            hashstr += len(i).to_bytes(4, "big") + i
        return HMAC(k, hashstr, hashlib.blake2b).digest()

    @staticmethod
    def privbytes(privkey):
        return privkey.private_bytes(Encoding.Raw, PrivateFormat.Raw, NoEncryption())

    @staticmethod
    def pubbytes(pubkey):
        return pubkey.public_bytes(
            Encoding.Raw,
            PublicFormat.Raw,
        )

    @staticmethod
    def rendezvouzkey(privkey, pubkey, role, debug=False):
        """
        creates the rendezvouz key from two 32-bit bytestrings

        Parameters
        ----------
        privkey : bytes[32]
            the private key bytestring

        pubkey : bytes[32]
            the public key bytestring

        role : boolean
            True : Alice
            False : Bob

        Returns
        ----------
        sharedkey : b''
            shared private key of length 32

        rendezvouzkey : b''
            key for the rendezvous of length 64
        """
        priv_x255 = X25519PrivateKey.from_private_bytes(privkey)
        # priv_x255 = Ed25519PrivateKey.from_private_bytes(privkey)
        if debug:
            print("private key:", briar.bytestostr(briar.privbytes(priv_x255)))
        pub_x255 = priv_x255.public_key()
        if debug:
            print("public key:", briar.bytestostr(briar.pubbytes(pub_x255)))
        other_pub_x255 = X25519PublicKey.from_public_bytes(pubkey)
        # other_pub_x255 = Ed25519PublicKey.from_public_bytes(pubkey)
        if debug:
            print("other public key:", briar.bytestostr(briar.pubbytes(other_pub_x255)))
        # Alice calculates raw_secret = DH(pri_a, pub_b)
        shared_key = priv_x255.exchange(other_pub_x255)
        if debug:
            print("shared key:", briar.bytestostr(shared_key))
        # static_master_key = HASH("org.briarproject.bramble.transport/STATIC_MASTER_KEY", raw_secret, pub_a, pub_b)
        # HASH(x_1, ..., x_n) = H(int_32(len(x_1)) || x_1 || ... || int_32(len(x_n)) || x_n)
        master_key = None
        # HASHING ORDER IS IMPORTANT. Both parties must agree which will fill
        # the role of 'alice' and 'bob' beforehand and remember it subsequently
        if role:
            master_key = briar.hashfunc(
                [
                    b"org.briarproject.bramble.transport/STATIC_MASTER_KEY",
                    shared_key,
                    briar.pubbytes(pub_x255),
                    briar.pubbytes(other_pub_x255),
                    b"0",
                ]
            )
        else:
            master_key = briar.hashfunc(
                [
                    b"org.briarproject.bramble.transport/STATIC_MASTER_KEY",
                    shared_key,
                    briar.pubbytes(other_pub_x255),
                    briar.pubbytes(pub_x255),
                    b"0",
                ]
            )
        if debug:
            print("master key:", briar.bytestostr(master_key))
        rendkey = briar.macfunc(
            master_key, [b"org.briarproject.bramble.rendezvous/RENDEZVOUS_KEY", b"0x00"]
        )
        if debug:
            print("rendezvous key:", briar.bytestostr(rendkey))
        return shared_key, rendkey

    @staticmethod
    def splitrendezvouskey(rkey):
        """
        splits the 64-byte rendezvous key into a secret key for alice (akey)
            and bob (bkey)

        Parameters
        -----------
        rkey : b''
            the 64-byte rendezvous keys

        Returns
        -----------
        akey : b''
            the 32-byte secret key for alice

        bkey : b''
            the 32-byte secret key for bob
        """
        akey = rkey[:32]
        bkey = rkey[32:]
        return akey, bkey

    @staticmethod
    def gettorkeys(privkey_a, pubkey_b):
        """
        returns the tor keys to be used by alice and bob, respectively, given
        their private and public keys, respectively.

        Parameters
        ----------
        privkey_a : b''
            32-length bytestring of the private key for alice

        pubkey_b : b''
            32-length bytestring of the public key of bob

        role : boolean (deprecated)
            this is required for cryptographic reasons.
            data for this field should be stored in memory by both clients
            when they initially exchange their contact information.
            one must be True, and the other must be False.

        Returns
        ---------
        '' (88), '' (88), '' (56), '' 56
            the tor public key for alice (a) to use for inbound connections
            from bob (b), and the tor public key for alice to use for outbound
            connections to bob
            then the .onion sid for alice, and the .onion sid for bob
            The first two keys will be the ingoing and outgoing keys, respectively.
            The second two keys will be the ingoing and outgoing tor addresses, respectively.
        """
        role = briar.determinerole(privkey_a, pubkey_b)
        skey, rkey = briar.rendezvouzkey(privkey_a, pubkey_b, role)
        if role:
            akey, bkey = briar.splitrendezvouskey(rkey)
        else:
            bkey, akey = briar.splitrendezvouskey(rkey)
        # print(base64.b32encode(akey))
        # print(base64.b32encode(bkey))
        ta, sa = tor.calculateonionhashes(akey)
        tb, sb = tor.calculateonionhashes(bkey)
        return ta, tb, sa, sb
        # return briar.gettorpublickey(akey), briar.gettorpublickey(bkey)

    @staticmethod
    def bytestostr(bytesstr):
        return (
            "(len: " + str(len(bytesstr)) + ") | " + base64.b64encode(bytesstr).decode()
        )

    @staticmethod
    def determinerole(pubkey_a, pubkey_b):
        """
        returns True if pubkey_a should be alice, False if not

        Parameters
        ---------
        pubkey_a : b''
            32-length bytestring public key

        pubkey_b : b''
            32-length bytestring public key

        Returns
        --------
        boolean
        """
        return briar.lt(pubkey_a, pubkey_b)

    @staticmethod
    def lt(pubkey_a, pubkey_b):
        """
        returns True if pubkey_a is less than b, False if not

        Parameters
        ---------
        pubkey_a : b''
            32-length bytestring public key

        pubkey_b : b''
            32-length bytestring public key

        Returns
        --------
        boolean
        """
        ba = bytearray(pubkey_a)
        bb = bytearray(pubkey_b)
        for i in range(len(pubkey_a)):
            if ba[i] < bb[i]:
                return True
            elif ba[i] > bb[i]:
                return False
        return False

    @staticmethod
    def getleastindex(arr, begin=0):
        least = begin
        for i in range(len(arr) - begin):
            if briar.lt(arr[i + begin], arr[least]):
                least = i + begin
        return least

    @staticmethod
    def sortkeys(keysarray):
        """
        sorts the keys in an array from least to greatest
        """
        sortarr = copy.copy(keysarray)
        for i in range(len(sortarr)):
            least = briar.getleastindex(sortarr, i)
            if i != least:
                sortarr[i], sortarr[least] = sortarr[least], sortarr[i]
        return sortarr

    @staticmethod
    def verifysignature(pubkey, sig, data):
        edkey = Ed25519PublicKey.from_public_bytes(pubkey)
        return edkey.verify(sig, data)


class tor:
    @staticmethod
    def expand_private_key(secret_key) -> bytes:
        # this follows the steps outlined in RFC 8032
        # https://datatracker.ietf.org/doc/html/rfc8032#section-5.1.5
        # see here: https://crypto.stackexchange.com/questions/99639/generating-and-validating-a-signature-with-ed25519-expanded-private-key
        # print('secret key:',base64.b64encode(secret_key))
        hash = hashlib.sha512(secret_key).digest()
        # print('s1:',base64.b64encode(hash))
        hash = bytearray(hash)
        # print('s2:',base64.b64encode(hash))
        hash[0] &= 248
        # print('s3:',base64.b64encode(hash))
        hash[31] &= 127
        # print('s4:',base64.b64encode(hash))
        hash[31] |= 64
        # print('s5:',base64.b64encode(hash))
        return bytes(hash)

    @staticmethod
    def calculateonionhashes(privbytes):
        """
        calculates the tor parameters for a given 32-bytes private key

        Parameters
        ----------
        privbytes : b'' (32)
            32-length bytestring containing the private key

        Returns
        ---------
        '' (88)
            base64 encoded tor public key to be passed to
            create_ephemeral_hidden_service()
        ''
            56-length base32 .onion service id (minus the .onion) where service can
            be reached.
        """
        expanded = tor.expand_private_key(privbytes)
        tbytes = base64.b64encode(expanded).decode()
        privkey = Ed25519PrivateKey.from_private_bytes(privbytes)
        pubkey = privkey.public_key().public_bytes(Encoding.Raw, PublicFormat.Raw)
        sid = HiddenServiceDescriptorV3.address_from_identity_key(pubkey, False)
        return tbytes, sid

    @staticmethod
    def onionfrompublic(pubbytes):
        return HiddenServiceDescriptorV3.address_from_identity_key(pubbytes, False)


class torcontroller:

    def __del__(self):
        if self.controller != None:
            self.controller.close()

    def __init__(self, password=None, address=None, port=None):
        print("p", password, "a", address, "po", port)
        if address != None:
            parsedaddress = (
                address if "." in address else torcontroller.ip_from_hostname(address)
            )
        else:
            parsedaddress = None
        print(
            "initializing tor controller with address:",
            address,
            "parsedaddress:",
            parsedaddress,
            "and port:",
            port,
        )
        if parsedaddress == None:
            if port == None:
                self.controller = Controller.from_port()
            else:
                self.controller = Controller.from_port(port=port)
        else:
            if port == None:
                self.controller = Controller.from_port(address=parsedaddress)
            else:
                self.controller = Controller.from_port(address=parsedaddress, port=port)
        if password != None:
            self.controller.authenticate(password)
        else:
            self.controller.authenticate()
        self.services = []

    @staticmethod
    def ip_from_hostname(hostname):
        return socket.gethostbyname(hostname)

    def maptorsocket(self, portmap, key, block=True):
        """
        maps a socket on tor to a previously unmapped socket on the local
        machine on a previously unmapped key

        Parameters
        ----------
        portmap : {}
            should be in the form
                {<external to port (int)>:<internal mapped port (int)>, ...}

        key : ''
            base64 string for a desired tor service's service key

        Returns
        ----------
        onion service descriptor
            handle to the onion service
        """
        print("mapping", portmap, "for tor key:", key)
        onion = self.controller.create_ephemeral_hidden_service(
            ports=portmap,
            discard_key=False,
            await_publication=block,
            key_type="ED25519-V3",
            key_content=key,
        )
        if not onion.is_ok():
            return -1
        print("mapped successfully.")
        self.services.append({"key": key, "sid": onion.service_id, "portmap": portmap})
        return onion

    def maptorsockets(self, portmap, keys):
        """
        asynchronously creates hidden services for all the hidden services in keys,
        and returns the hidden service handles in the format:
        {
            <key>: <hidden service object>
        }
        """
        outqueue = queue.Queue()
        threads = []
        stime = dt.now()
        for i in keys:
            key = BriarKey()
            nkey = BriarKey()
            p = key.private_transport()
            # print('private:',p)
            b = nkey.public_transport()
            # print('public:',b)
            a, b, oa, ob = briar.gettorkeys(
                key.private_transport(), nkey.public_transport()
            )
            # o = tc.maptorsocket([externalport, mappingport], b, False)
            x = threading.Thread(
                target=lambda q: q.put([i, self.maptorsocket(portmap, i, False)]),
                args=(outqueue,),
            )
            # print('starting thread:',i)
            x.start()
            threads.append(x)
        for i, j in enumerate(threads):
            # print('joining thread:',i)
            j.join()
        etime = dt.now()
        outp = {}
        while outqueue.qsize() > 0:
            # print('service created:',outqueue.get().service_id)
            e = outqueue.get()
            print(e)
            outp[e[0]] = e[1]
        return outp

    def getserviceindex(self, key):
        """
        get the index of a tor service stored in memory

        Parameters
        ----------
        key : b''
            bytestring of the private key for the tor service

        Returns
        ----------
        int
            index of the tor service
        """
        try:
            return [i["key"] for i in self.services].index(key)
        except Exception:
            return -1

    def getservice(self, key):
        """
        gets the information about a tor service stored in memory

        Parameters
        ----------
        key : b''
            service key for the tor service

        Returns
        {}
            dictionary containing all information stored about the tor service
            -1 if nothing could be found.
        """
        ind = self.getserviceindex(key)
        if ind == -1:
            return ind
        return self.services[ind]

    def closetorservice(self, key):
        """
        close the service of the given tor key

        Parameters
        ----------
        key : b''
            bytestring of the relevent tor hidden service

        Returns
        ----------
        boolean
            whether or not the operation was successful
        """
        ind = self.getserviceindex(key)
        if not ind == -1:
            s = self.services[ind]
            self.controller.remove_ephemeral_hidden_service(s["sid"])
            self.services.pop(ind)
            return True
        return False

    def changeportmap(self, ports, key):
        """
        changes the port map of a given key to the one provided

        Parameters
        ----------
        ports : {}
            should be in the form
                {<external to port (int)>:<internal mapped port (int)>, ...}

        key : b''
            raw bytestring for a desired tor service's private key

        Returns
        ----------
        onion service descriptor
            handle to the onion service

        """
        self.closetorservice(key)
        s = self.maptorsocket(ports, key)
        return s

    def close(self):
        self.__del__()


class torserver:
    def __init__(
        self, key, defaultport_internal=13370, defaultport_external=13370, torpass=None
    ):
        self.key = key
        self.dfport_internal = defaultport_internal
        self.dfport_external = defaultport_external
        self.torpass = torpass
        self.tc = torcontroller(torpass)

    def addconnection(self, pubkey, callback):
        keyba, keyab = briar.gettorkeys(self.key.private_transport(), pubkey)
        o = self.tc.maptorsocket(
            {self.defaultport_external: self.defaultport_internal}, keyba, True
        )
        return o.service_id


def testmakerkey():
    """
    step through the process of creating the rendezvous key and print the
    results
    """
    key1 = BriarKey(seedphrase="test1")
    key2 = BriarKey(seedphrase="test2")
    prkey1 = key1.private_transport()
    prkey2 = key2.private_transport()
    pubkey1 = key1.public_transport()
    pubkey2 = key2.public_transport()
    print("key 1:")
    print(key1)
    print("key 2:")
    print(key2)
    # print('private key 1:',briar.bytestostr(prkey1))
    # print('private key 2:',briar.bytestostr(prkey2))
    # print('public key 1:',briar.bytestostr(pubkey1))
    # print('public key 2:',briar.bytestostr(pubkey2))
    print(divider)
    skey1, rkey1 = briar.rendezvouzkey(prkey1, pubkey2, True, True)
    print(divider)
    skey2, rkey2 = briar.rendezvouzkey(prkey2, pubkey1, False, True)
    print(divider)
    print("secret key1:", briar.bytestostr(skey1))
    print("rendezvous key1:", briar.bytestostr(rkey1))
    print(divider)
    print("secret key2:", briar.bytestostr(skey2))
    print("rendezvous key2:", briar.bytestostr(rkey2))
    print(divider)
    akey, bkey = briar.splitrendezvouskey(rkey1)
    tkeya1, tkeyb1, o1, o2 = briar.gettorkeys(prkey1, pubkey2)
    tkeya2, tkeyb2, o3, o4 = briar.gettorkeys(prkey2, pubkey1)
    print("bob->alice for alice:", tkeya1)
    print("alice->bob for alice:", tkeyb1)
    print("alice's .onion for alice:", o1)
    print("alice's .onion for bob:", o2)
    print("alice->bob for bob:", tkeya2)
    print("bob->alice for bob:", tkeyb2)
    print("bob's .onion for bob:", o3)
    print("bob's .onion for alice:", o4)
    print(divider)
    """controller = Controller.from_port()
    controller.authenticate('apples123')
    onion = controller.create_ephemeral_hidden_service(
        ports={80:5000},
        discard_key=False,
        await_publication=True,
        key_type='ED25519-V3',
        key_content=rkeya1.decode()
    )
    print('onion created.')
    print(onion.service_id)
    a = input('hit enter to exit')"""
    return tkeya1, tkeyb1, tkeya2, tkeyb2


def tor_connect(host, port, torserver=None):
    if torserver == None:
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050, True)
    else:
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, torserver, 9050, True)
    s = socks.socksocket()
    s.connect((host, port))
    return s


def mainfunc():
    # create_hidden_service()
    testmakerkey()


def testtor():
    # s = createtorsocket(onionaddy, 80)
    # s.send('testing\n'.encode())
    # s.close()
    portsmap = {80: 5000}
    prkey1 = X25519PrivateKey.generate()
    prkey2 = X25519PrivateKey.generate()
    pubkey1 = prkey1.public_key()
    pubkey2 = prkey2.public_key()
    print(divider)
    print("created private key:", briar.bytestostr(briar.privbytes(prkey1)))
    print(divider)
    print("created other public key:", briar.bytestostr(briar.pubbytes(pubkey2)))
    skey, rkey = briar.rendezvouzkey(
        briar.privbytes(prkey1), briar.pubbytes(pubkey2), False
    )
    print(divider)
    print("secret key:", briar.bytestostr(skey))
    print(divider)
    print("rendezvous key:", briar.bytestostr(rkey))
    # akey will be the private key for incoming bytes for alice from bob
    akey = rkey[:32]
    # bkey will be the private key for incoming bytes for bob from alice
    bkey = rkey[32:]
    print(divider)
    print("akey:", briar.bytestostr(akey))
    print(divider)
    print("bkey:", briar.bytestostr(bkey))

    # controller = Controller.from_port(address='127.0.0.1', port=18098)
    controller = Controller.from_port(address="127.0.0.1", port=9051)
    controller.authenticate("password123")
    # controller.authenticate()

    print("public key to use")
    tkey = getpublickey(akey)
    print(tkey)
    print(bytestostr(tkey))
    print(type(tkey))
    print("example key")
    tkey2 = tor_keys_from_seed("test")
    print(tkey2)
    print(bytestostr(tkey2))
    print(type(tkey2))
    onion = controller.create_ephemeral_hidden_service(
        ports=portsmap,
        discard_key=False,
        await_publication=True,
        key_type="ED25519-V3",
        key_content=tkey.decode(),
    )
    print("onion created.")
    print(onion.service_id)


def tortest2():
    k1 = BriarKey("test1")
    k2 = BriarKey("test2")
    print("key1:")
    print(k1)
    print("key2:")
    print(k2)
    print(divider)
    ik, ok, ia, oa = briar.gettorkeys(k1.private_transport(), k2.public_transport())
    print("alice keys:")
    print("server key:", ik)
    print("client key:", ok)
    print(divider)
    tc = torcontroller("password123")
    print(dt.now(), "creating onion services")
    # p1 = tc.maptorsocket({81: 5001}, ik)
    p1 = tc.maptorsocket({13370: 13370}, ik)
    # print(dt.now(), "created first onion service")
    # tc.changeportmap({80: 5000, 81: 5001}, s1)
    # print(dt.now(), "changed portmap")
    # p2 = tc.maptorsocket({81:5001}, s1)
    # print(dt.now(),'created second onion service')
    print("address 1:", p1.service_id)
    # print('address 2:',p2.service_id)
    print(divider)
    a = input("hit enter to exit")


def test_BriarKey():
    bkey = BriarKey(seedphrase="poop")
    print(bkey)


def testhashfunc():
    a = b"test1"
    b = b"test2"
    c = b"test3"
    print(base64.b64encode(briar.hashfunc([a, b, c])))
    print(base64.b64encode(briar.hashfunc([b, a, c])))


def testgrouphash():
    # a = [4, 6, 9, 2, 4, 6, 1]
    # print(briar.getgrouphash(a))
    a = BriarKey(seedphrase="test1").public()
    b = BriarKey(seedphrase="test2").public()
    c = BriarKey(seedphrase="test3").public()
    testarr = [a, b, c]
    testkey = briar.getgrouphash("dicks", testarr)
    print(briar.bytestostr(testkey))


def testsign():
    seed1 = "test1".encode()
    seed2 = "test2".encode()
    data = "testing".encode()
    print("data:", data)
    privkey1 = bytes(hashlib.sha256(seed1).digest())
    privkey2 = bytes(hashlib.sha256(seed2).digest())
    key1 = Ed25519PrivateKey.from_private_bytes(privkey1)
    key2 = Ed25519PrivateKey.from_private_bytes(privkey2)
    sig = key1.sign(data)
    pubkey1 = key1.public_key()
    pubkey2 = key2.public_key()
    print(pubkey1.verify(sig, data))
    print(pubkey2.verify(sig, data))


def testsign2():
    data = "testing".encode()
    key1 = BriarKey(seedphrase="test1")
    sig = key1.sign(data)
    print(briar.verifysignature(key1.public_signing(), sig, data))


def testconvert():
    seed1 = "test1".encode()
    seed2 = "test2".encode()
    data = "testing".encode()
    print("data:", data)
    privkey1 = bytes(hashlib.sha256(seed1).digest())
    privkey2 = bytes(hashlib.sha256(seed2).digest())
    sk1 = Ed25519PrivateKey.from_private_bytes(privkey1)
    pbytes = sk1.public_key().public_bytes(
        Encoding.Raw,
        PublicFormat.Raw,
    )
    print("ed25519 public key:", briar.bytestostr(pbytes))
    convertedpublic = crypto_sign_ed25519_pk_to_curve25519(pbytes)
    print("x25519 public key:", briar.bytestostr(convertedpublic))
    sk2 = X25519PrivateKey.from_private_bytes(privkey1)
    pk2 = sk2.public_key().public_bytes(
        Encoding.Raw,
        PublicFormat.Raw,
    )
    print("expected x25519 public key:", briar.bytestostr(pk2))


def testcreatemultipleaddresses():
    key1 = BriarKey(seedphrase="test1")
    key2 = BriarKey(seedphrase="test2")
    key3 = BriarKey(seedphrase="test3")
    createtor = True
    rkey121, rkey112, o121, o122 = briar.gettorkeys(
        key1.private_transport(), key2.public_transport()
    )
    rkey131, rkey113, o131, o133 = briar.gettorkeys(
        key1.private_transport(), key3.public_transport()
    )
    print(divider)
    print("keypair 1:")
    print(rkey121, "->", o121)
    print(rkey112, "->", o122)
    print(divider)
    print("keypair 2:")
    print(rkey131, "->", o131)
    print(rkey113, "->", o133)
    print(divider)
    tc = None
    if createtor:
        print("creating tor sockets")
        tc = torcontroller("apples123")
        print(dt.now(), "creating first tor connection on", rkey121)
        o1 = tc.changeportmap({13370: 13370}, rkey121)
        print("onion created:", o1.service_id + ".onion")
        print(dt.now(), "creating second tor connection on", rkey131)
        o2 = tc.changeportmap({13370: 13370}, rkey131)
        print("onion created:", o2.service_id + ".onion")
    a = input("hit enter to exit")
    tc.close()


if __name__ == "__main__":
    # mainfunc()
    addr = "2urlskvh3eksjcuqa7zcjr7hh3txlqyoc7iq6n5l7c44qttpht3el6qd.onion"
    tortest2()
    # testmakerkey()
    # testcreatemultipleaddresses()
    # testsign()
    # testsign2()
    # testconvert()
    # testgrouphash()
    # tortest2()
    # testhashfunc()
    # testBriarKey()
    # testtor()
    pass
