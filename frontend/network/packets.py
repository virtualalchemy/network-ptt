import base64
import copy
import json

import crc8

lengthsize = 2
import pickle

messagemappings = {
    "audio": 1,
    "text": 2,
    "image": 3,
    "file": 4,
    "server_create": 5,
    "relay_message": 6,
}


def bytestobase64(bytestring: bytes) -> str:
    return base64.b64encode(bytestring).decode("UTF-8")


def base64tobytes(base64str: str) -> bytes:
    return base64.b64decode(base64str.encode("UTF-8"))


def parsepacket(packetbytes):
    pack = packet(packetbytes)
    return pack.getmessage()


class generalizedpacket:
    def __init__(self, dict):
        self.contents = dict

    def getcontents(self):
        outp = copy.copy(self.contents)
        for i in outp:
            if type(outp[i]) == type(b""):
                outp[i] = base64.b64encode(outp[i]).decode("UTF-8")
        return json.dumps(outp)

    @staticmethod
    def isbase64(contents):
        # print(contents)
        # print(base64.b64decode(contents))
        # print(base64.b64encode(base64.b64decode(contents)))
        if type(contents) == type(""):
            try:
                if (
                    base64.b64encode(base64.b64decode(contents)).decode("UTF-8")
                    == contents
                ):
                    # print('detected')
                    return True
            except:
                pass
        return False

    @staticmethod
    def parsepacket(contents):
        outp = json.loads(contents)
        for i in outp:
            if generalizedpacket.isbase64(outp[i]):
                outp[i] = base64.b64decode(outp[i])
        return generalizedpacket(outp)

    def todict(self):
        return self.contents

    def __str__(self):
        return str(self.contents)


class datapacket:
    """
    | type		|	: 1
    | source	|	: 32
    | dest		|	: 32
    | payload	|	: length-71
    """

    def __init__(self, type, source, dest, message):
        self.type = type
        self.source = source
        self.dest = dest
        self.message = message

    def getcontents(self):
        return self.source + self.dest + self.type.to_bytes(1, "little") + self.message

    def todict(self) -> dict:
        return {
            "source": self.source,
            "dest": self.dest,
            "type": self.type,
            "payload": self.message,
        }

    def __str__(self):
        dict = self.todict()
        for i in dict:
            if type(dict[i]) == type(b""):
                dict[i] = dict[i].decode()
        return json.dumps(dict, indent=4)

    def repr_serializable(self):
        dict = self.todict()
        for i in dict:
            if type(dict[i]) == type(b""):
                dict[i] = base64.b64encode(dict[i]).decode("UTF-8")
        return json.dumps(dict)

    @staticmethod
    def parsedatapacket(messagebytes):
        print("messagebytes:", messagebytes)
        print(type(messagebytes))
        return datapacket(
            messagebytes[64], messagebytes[0:32], messagebytes[32:64], messagebytes[65:]
        )


class packet:
    """
    packet format:
    | header	|	: 1
    | length	|	: 2
    | body		|	: length - 4
        | type		|	: 1
        | source	|	: 32
        | dest		|	: 32
        | payload	|	: length-71
    | crc		|	: 2
    | footer	|	: 1
    """

    contents = None
    overheadbytes = 5
    messagetype = 1

    @staticmethod
    def crc(bytestr):
        hash = crc8.crc8()
        hash.update(bytestr)
        h = hash.hexdigest()
        return bytes.fromhex(h)

    def __init__(self, bytestr):
        if type(bytestr) != type(b"test"):
            self.message = bytes(bytestr)
        else:
            self.message = bytestr

    @staticmethod
    def getcontents(message):
        global lengthsize
        outp = (
            int(1).to_bytes(1, "little")
            + (len(message) + packet.overheadbytes).to_bytes(lengthsize, "little")
            + message
            + packet.crc(message)
            + int(4).to_bytes(1, "little")
        )
        return outp

    @staticmethod
    def getpayload(contents):
        return contents[3:-2]

    @staticmethod
    def getmessagelength(contents):
        global lengthsize
        return int.from_bytes(contents[1:3], "little")

    def getmessage(self):
        return self.message

    @staticmethod
    def checkcrc(contents):
        # return int.from_bytes(self.contents[-2], 'little') == self.crc(self.getmessage())
        # print(self.getmessage())
        # print(int.from_bytes(self.crc(self.getmessage()), 'little'))
        lval = contents[-2]
        rval = int.from_bytes(packet.crc(packet.getpayload(contents)), "little")
        # print('lval:',lval)
        # print('rval:',rval)
        return lval == rval

    @staticmethod
    def isvalid(contents):
        beginch = contents[0] == 1
        endch = contents[-1] == 4
        lench = len(contents) == packet.getmessagelength(contents)
        crcch = packet.checkcrc(contents)
        # print('begin:',beginch)
        # print('end:', endch)
        # print('lench:', lench)
        # print('crcch:',crcch)
        if beginch and endch and lench and crcch:
            return True
        return False

    def printstr(self, divider=10):
        outp = ""
        for i, j in enumerate(self.getcontents(self.message)):
            outp += str(i) + ":" + str(int(j)) + "\t"
            if i != 0 and i % divider == 0:
                outp += "\n"
        return outp

    def __str__(self):
        return str(self.getcontents(self.message)) + "\n" + self.printstr()

    @staticmethod
    def parsepacket(contents):
        if packet.isvalid(contents):
            return packet(packet.getpayload(contents))
        else:
            raise ValueError("Packet is not valid:" + contents.decode())

    @staticmethod
    def parsedatapacket(bytestr):
        p = packet.parsepacket(bytestr)
        return datapacket.parsedatapacket(p.getmessage())


def testdatapacket():
    divider = "-" * 75
    print(divider)
    print("basic test:")
    testb = b"\1testing"
    pack = packet(testb)
    print("packet:\n", pack)
    print(divider)
    dp = datapacket(1, b"a" * 32, b"b" * 32, b"testing")
    print("datapacket:", dp)
    p = packet(dp.getcontents())
    print("packet:\n", p)
    print("resulting datapacket:")
    conts = p.getcontents(dp.getcontents())
    # print('conts:',conts)
    # print(type(conts))
    print(p.parsedatapacket(conts))


def testgeneralizedpacket():
    dat = {"type": 1, "address": b"12345", "command": "create"}
    p = generalizedpacket(dat)
    print("packet:", p)
    print("contents:", p.getcontents())
    p2 = generalizedpacket.parsepacket(p.getcontents())
    print("packet after importing:", p2)


def testisbase64():
    test = "MTIzNDU="
    print(generalizedpacket.isbase64(test))


if __name__ == "__main__":
    # testdatapacket()
    testgeneralizedpacket()
    # testisbase64()
