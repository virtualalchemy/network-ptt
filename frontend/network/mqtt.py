import base64
import json
import logging
import os

import paho.mqtt.client as mqtt
from db.db import dbwrapper

from libs.keyhandler import writekeys
from network.onionnetworking import BriarKey, tor_connect
from network.packets import base64tobytes, bytestobase64, datapacket, generalizedpacket

"""
This file contains the mqtt abstraction layer that handles the communication between
the backend and the frontend. 

When a message is received:
* mqttwrapper will insert the message contents into the database.
* mqttwrapper will publish a message to the environment variable INCOMINGTOPIC topic
	on the mqtt server.
	* this message will be a json-formatted string in the following form:
		{
			'id': int
			'source': str
			'type': int
		}
	* the frontend will be notified of this if it is subscribed to the same topic on the 
		mqtt server. It will then be able to decide how it will handle this information.
When a message is sent:
* the frontend will publish a message to the OUTGOINGTOPIC environment variable on the
	mqtt server.
	* this message will be the output of network.packet.datapacket.getcontents()
* the backend will then take care of handling the appropriate crypto operations,
	routing, etc.
"""


class mqttwrapper:
    """
    abstracts the mqtt functions of the app into something more stateful and manageable
    """

    topics = None

    def __init__(self, host: str, port: int):
        """
        Parameters
        ----------
        host : str
                url for the mqtt server

        port : int
                port to address the server on
        """
        # print('Creating MQTT wrapper for',host,':',port)
        logging.debug("Creating MQTT wrapper for" + host + ":" + str(port))
        self.mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
        self.mqttc.on_connect = self.on_connect
        self.mqttc.on_message = self.on_message

        self.mqttc.connect(host, port, 60)
        self.topics = {
            "outgoing": os.getenv("OUTGOINGTOPIC"),
            "control": os.getenv("CONTROLTOPIC"),
        }
        self.mqttc.loop_start()
        self.db = dbwrapper()

    def close(self):
        """
        close the mqtt abstraction layer
        """
        self.mqttc.loop_stop()
        self.mqttc.disconnect()

    def __del__(self):
        self.close()

    def on_connect(self, client, userdata, flags, reason_code, properties) -> None:
        """
        The callback for when the client receives a CONNACK response from the server.
        """
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        # client.subscribe("$SYS/#")
        for i in self.topics:
            client.subscribe(self.topics[i])

    def handlecontrol(self, pack: network.generalizedpacket) -> None:
        """
        handles the logic for when a control message is received from a frontend
        """
        dat = json.loads(pack.getcontents())
        logging.debug("Received control message: " + str(pack.getcontents()))
        match dat["type"]:
            case "create_contact":
                self.db.importcontact(
                    dat["username"], dat["pubkey_transport"], dat["pubkey_signing"]
                )
            case "create_keys":
                bkey = BriarKey(dat["seed"], username=dat["username"])
                writekeys(os.getenv("KEYSFILE"), dat["username"], dat["seed"])
                self.db.recalculateuserkeys(bkey)
            case "get_contacts":
                props = msg.properties
                if not hasattr(props, "ResponseTopic") or not hasattr(
                    props, "CorrelationData"
                ):
                    logging.warning(
                        "ERROR: get_contacts requested but no reply requested."
                    )
                    return
                corr_id = props.CorrelationData
                reply_to = props.ResponseTopic
                props = mqtt.Properties(PacketTypes.PUBLISH)
                props.CorrelationData = corr_id
                conts = [i.todict() for i in self.dbwrapper.getcontacts()]
                payload = json.dumps(conts)
                self.mqttc.publish(reply_to, payload, qos=1, properties=props)

    def handlemessage(self, pack: network.datapacket) -> None:
        """
        handles the logic for when an outbound communications message is received from the frontend
        """
        dat = pack.todict()
        # print('message:')
        # print(pack.getcontents())
        # get the outbound tor address corresponding to the public trasport key
        address = self.db.getcontactbypublictransport(bytestobase64(dat["dest"]))
        # print('attempting to send to address:',address)
        logging.debug("Attempting to send outbound message to: " + str(address))
        # call tor_connect from onionnetworking to get a socket
        try:
            s = tor_connect(address, 13370)
            s.send(pack.getcontents())
            s.close()
        except Exception as e:
            # print('could not send:')
            logging.warning("Could not send message:" + str(e))
            # print(e)

    # The callback for when a PUBLISH message is received from the server.
    # HANDLE RECEIVED MESSAGES FROM A FRONTEND HERE
    def on_message(self, client, userdata, msg) -> None:
        """
        This function handles the received messages from a frontend and will be called
        every time a message is received via MQTT
        """
        logging.info("Received MQTT message: " + msg.topic + " " + str(msg.payload))
        # print(msg.topic+" "+str(msg.payload))
        if msg.topic == self.topics["control"]:
            pack = generalizedpacket.parsepacket(msg.payload)
            # print('received generalized packet:',pack.getcontents())
            self.handlecontrol(pack)
        elif msg.topic == self.topics["outgoing"]:
            dp = datapacket.parsedatapacket(msg.payload)
            # print('received outgoing datapacket:')
            # print(dp.todict())
            self.handlemessage(dp)

    def publish_message_frontend(self, id: int, source: str, type: int) -> bool:
        """
        publish a message notification via mqtt for any listening frontends.

        Parameters
        ----------
        id : int
        source : string
        type : int
        """
        payload = {"id": id, "source": bytestobase64(source), "type": type}
        success = self.mqttc.publish(os.getenv("INCOMINGTOPIC"), json.dumps(payload))
        logging.info(
            "published message from source: ",
            +source + " and type: " + str(type) + " and id: ",
            str(id),
        )
        # print('published message from',source,'of type',type,'and id',id,'success:',success)
        return success
