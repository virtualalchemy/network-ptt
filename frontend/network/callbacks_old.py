import os
from dataclasses import dataclass
from typing import List
from urllib.parse import urljoin

import requests

from network.packets import datapacket


@dataclass
class Message:
    content: str
    content_type: str


API_ENDPOINT = os.getenv("API_ENDPOINT", "http://backend:8000")


def post_message(dpacket: datapacket) -> None:
    url = urljoin(API_ENDPOINT, "/messages")
    data = dpacket.todict()
    r = requests.post(url, data)
    r.raise_for_status()


def mark_message_as_sent(message_id: int) -> None:
    url = urljoin(API_ENDPOINT, f"/messages/{message_id}")
    data = {"is_sent": True}
    r = requests.put(url, data)
    r.raise_for_status()


def get_messages() -> List[dict]:
    url = urljoin(API_ENDPOINT, "messages/unsent")
    r = requests.get(url)
    r.raise_for_status()
    return r.json()
