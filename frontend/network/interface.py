import base64
import json
import os
import signal
import socket
import threading
import time

import meshtastic
from meshtastic.serial_interface import SerialInterface
from pubsub import pub
from twisted.internet import protocol, reactor

from network.onionnetworking import briar, tor_connect, torcontroller
from network.packets import datapacket, packet

divider = "-" * 70
broadcast = b"f" * 32
ackmask = 40


class interface(threading.Thread):
    """
    Defines the base class for a networking interface. Classes implementing this
    template should call onreceive() when packets are received. They should also
    reimplement the run() function.
    """

    def __init__(self, callback, *args):
        super().__init__()
        self.registercallback(callback)
        self.running = False
        signal.signal(signal.SIGINT, self.close)
        signal.signal(signal.SIGTERM, self.close)

    def registercallback(self, callback):
        self.callback = callback

    def onreceive(self, message):
        self.callback(message)

    def run(self):
        self.running = True
        while self.running:
            time.sleep(1)

    def stop(self):
        print("stopping gracefully...")
        self.running = False


class meshtasticinterface(interface):
    """
    interface using the meshtastic radio
    """

    def __init__(self, callback, deventry=None, *args):
        """
        contructor for meshtasticinterface

        Parameters
        ----------
        callback : (function)
            handle to the function that will be called when a packet is received
        deventry : ''
            /dev entry to the radio interface (or COM port if on windows (cringe))
        """
        super().__init__(callback, args)
        print("meshtastic interface created. dev entry:", deventry)
        if not deventry == None:
            self.device = SerialInterface(devPath=deventry)
        else:
            self.device = SerialInterface()
        pub.subscribe(self.onreceive, "meshtastic.receive")
        signal.signal(signal.SIGINT, self.close)
        signal.signal(signal.SIGTERM, self.close)

    def getinfo(self):
        """
        wrapper for meshtastic getinfo() function
        """
        return self.device.getMyNodeInfo()

    def senddata(self, type, source, destination, data):
        """
        send data over the radio link.

        Parameters
        ----------
        type : int
            type of message
        destination : b'' (32)
            32 byte destination address
        data : b''
            payload data for the packet
        """
        pack = datapacket(type, source, destination, data)
        radioaddy = broadcast
        if radioaddy == broadcast:
            self.device.sendData(pack.getcontents())
        else:
            self.device.sendData(
                pack.getcontents(), destinationId=base64.b64encode(destination).decode()
            )

    def onreceive(self, packet, interface, address):
        """
        reimplementation of parent class onreceive()
        """
        # pack = datapacket.parsedatapacket(packet["decoded"]["payload"])
        self.callback(packet["decoded"]["payload"])

    def todict(self):
        return self.getinfo()

    def __str__(self):
        return json.dumps(self.todict(), indent=4)

    def close(self):
        self.device.close()
        self.stop()


class networkingprotocol(protocol.Protocol):
    def dataReceived(self, data):
        print("datareceived:", data)
        pub.sendMessage("networkreceived", data=data)


class serverfactory(protocol.Factory):
    protocol = networkingprotocol


class networkinterface(interface):
    def __init__(self, port, callback, *args):
        """
        constructor for the networking interface

        Parameters
        ----------
        callback : function
            function to be called when a packet is received
        port : int
            port to listen on
        """
        super().__init__(callback, *args)
        self.port = port
        self.callback = callback
        signal.signal(signal.SIGINT, self.close)
        signal.signal(signal.SIGTERM, self.close)

    def onreceive(self, data):
        # pack = datapacket.parsedatapacket(data)
        print("received data:")
        print(data)
        self.callback(data)

    def run(self):
        pub.subscribe(self.onreceive, "networkreceived")
        factory = serverfactory()
        reactor.listenTCP(self.port, factory)
        reactor.run(0)

    def close(self):
        reactor.stop()
        pub.unsubAll()
        self.stop()

    @staticmethod
    def createsocket(host, port):
        s = socket.socket()
        s.connect((host, port))
        return s

    def createtorsocket(host, port):
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050, True)
        s = socks.socksocket()
        s.connect((host, port))
        return s

    @staticmethod
    def senddata(host, port, data, tor=False):
        s = None
        if not tor:
            s = networkinterface.createsocket(host, port)
        else:
            s = createtorsocket(host, port)
        s.send(data)
        s.close()


def senddata(type, source, dest, message, privkey=None, toraddress=None):
    if privkey != None:
        # the public key for source, the public key for dest,
        # the onion key for source, the onion key for dest, respectively
        print("prkey:", privkey, "pbkey:", dest)
        pks, pkd, oks, okd = briar.gettorkeys(privkey, dest)
        print("calculated addresses:")
        print(json.dumps({"pks": pks, "pkd": pkd, "oks": oks, "okd": okd}, indent=4))
        destaddy = okd + ".onion"
    elif toraddress != None:
        destaddy = toraddress + ".onion"
    print("calling senddata() to:", destaddy)
    s = tor_connect(destaddy, int(os.getenv("TOR_MESSAGEPORT")))
    s.sendall(datapacket(type, source, dest, message).getcontents())
    s.close()


def testcb(packet):
    print("callback called:", packet)


def testiface():
    i = meshtasticinterface(b"a" * 32, testcb, "/dev/ttyACM0")
    print(i.getinfo())
    i.start()
    for j in range(5):
        print("main thread output")
        time.sleep(1)
    i.stop()


def testsenddatamesh():
    dev1entry = "/dev/ttyACM0"
    dev2entry = "/dev/ttyACM1"
    add1 = b"a" * 32
    add2 = b"b" * 32
    data = b"testing"
    dev1 = meshtasticinterface(add1, testcb, "/dev/ttyACM0")
    divider = "-" * 50
    print(divider)
    print("first interface")
    print(dev1)
    print(divider)
    print("second interface")
    dev2 = SerialInterface(devPath=dev2entry)
    print(dev2.getMyNodeInfo())
    print(divider)
    print("subscribing to topic")
    pub.subscribe(testcb, "meshtastic.receive")
    time.sleep(1)
    print("sending data")
    dev1.senddata(1, add2, data)
    # dev1.device.sendText('testing')
    print("done sending")
    a = input("hit enter to exit")
    dev1.close()
    dev2.close()


def testreceivedatamesh():
    dev1entry = "/dev/ttyACM0"
    dev2entry = "/dev/ttyACM1"
    add1 = b"a" * 32
    add2 = b"b" * 32
    data = b"testing"
    dev1 = meshtasticinterface(add1, testcb, dev1entry)
    divider = "-" * 50
    print(divider)
    print("first interface")
    print(dev1)
    print(divider)
    print("second interface")
    dev2 = meshtasticinterface(add2, testcb, dev2entry)
    print(dev2)
    print(divider)
    print("subscribing to topic")
    time.sleep(1)
    print("sending data")
    dev1.senddata(1, add2, data)
    # dev1.device.sendText('testing')
    print("done sending")
    a = input("hit enter to exit")
    dev1.close()
    dev2.close()


def testsenddata():
    host = "localhost"
    port = 13370
    s = networkinterface.createsocket(host, port)
    s.send(b"testing\n")


def testreceivedata():
    host = "localhost"
    port = 13370
    add1 = b"a" * 32
    add2 = b"b" * 32
    data = b"testing"
    pack = datapacket(1, add1, add2, data)
    print("creating network interface")
    ni = networkinterface(add1, testcb, port)
    print("starting network interface")
    ni.start()
    print("creating socket")
    s = networkinterface.createsocket(host, port)
    print("sending data")
    s.send(pack.getcontents())
    print("closing")
    a = input("hit enter to close\n")
    ni.close()


if __name__ == "__main__":
    # ifacetest()
    # testsenddata()
    testreceivedata()
