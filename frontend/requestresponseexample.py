"""
info on using mqtt in request-response mode:
    https://www.hivemq.com/blog/mqtt5-essentials-part9-request-response-pattern/
example code from here: 
    https://github.com/PrimeshShamilka/mqttv5_request_response_pattern
"""

import os
import random
import sys

import flet as ft
import pydenticon as pyd

sys.path.append(".")
import binascii
import time

import decouple
import paho.mqtt.client as mqtt
from db.db import dbwrapper
from db.models import Contact, Message
from decouple import config
from paho.mqtt.packettypes import PacketTypes
from paho.mqtt.properties import Properties

from network.packets import base64tobytes, bytestobase64

iconsize = 1024
iconsfile = "assets/contact_icons/"
defaulticon = "assets/icon.png"
mqhost = "localhost"
mqport = 17896
divider = "-" * 100

dbwrap = dbwrapper()
pydgen = pyd.Generator(10, 10, foreground=["#000000"], background="#0000ff")
mqttc = None
INCOMINGTOPIC = config("INCOMINGTOPIC")
OUTGOINGTOPIC = config("OUTGOINGTOPIC")
CONTROLTOPIC = config("CONTROLTOPIC")
ascii_letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


def randomsequence(leng):
    return "".join([random.choice(ascii_letters) for i in range(leng)])


clientid = randomsequence(10)
topics = [INCOMINGTOPIC, OUTGOINGTOPIC, CONTROLTOPIC, clientid]

print("clientid:", clientid)


def executecommand(mqttc, topic, command):
    """
    posts a message via mqtt on a given topic with given message contents, blocks for a
    response, and returns the result
    """
    correlation = randomsequence(5)
    props = Properties(PacketTypes.PUBLISH)
    # props.UserProperty = ("responsetopic", "2")
    props.ResponseTopic = bytes(clientid, "UTF-8")
    props.CorrelationData = bytes(correlation, "UTF-8")
    # props.CorrelationData = correlation
    mqttc.publish(topic=topic, payload=command, qos=1, properties=props)


def getcontacts():
    return [i[0] for i in dbwrap.getcontacts()]


def hashtoiconname(hashstring):
    outp = ""
    for i in hashstring:
        if i.isalnum():
            outp += i
    return outp


def loadicons():
    conts = getcontacts()
    for i in conts:
        with open(iconsfile + hashtoiconname(i.pubkey_transport) + ".png", "wb+") as f:
            # print('contact',i)
            f.write(pydgen.generate(i.pubkey_transport, iconsize, iconsize))


def getconversation(pubkey_transport):
    mes = dbwrap.getconversation(pubkey_transport=pubkey_transport)
    # print('finding messages')
    # print(mes)
    # exit(0)
    return mes


def getmessages(pubkey_transport):
    mes = dbwrap.getmessages(pubkey_transport=pubkey_transport)
    # print('finding messages')
    # print(mes)
    # exit(0)
    return mes


currentcontact = None
messagefield = None


def on_connect(client, userdata, flags, reason_code, properties) -> None:
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    for i in topics:
        client.subscribe(i)


def on_message(client, userdata, msg) -> None:
    """
    This function handles the received messages from a frontend and will be called
    every time a message is received via MQTT
    """
    print("received message via mqtt:", msg.topic, msg.payload)
    print("properties:", msg.properties)
    propsjson = msg.properties.json()
    if len(propsjson) > 0:
        print(msg.properties.CorrelationData)
        if "ResponseTopic" in propsjson:
            props = Properties(PacketTypes.PUBLISH)
            props.CorrelationData = msg.properties.CorrelationData
            if msg.properties.ResponseTopic == clientid:
                mqttc.publish(
                    topic=msg.properties.ResponseTopic,
                    payload="this is the response!",
                    qos=1,
                    properties=props,
                )
        else:
            print("received response!")


def sendmessage(message):
    print("sending message:", message)

    mqttc.publish(OUTGOINGTOPIC, message)


def init():
    global mqttc

    loadicons()
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, protocol=mqtt.MQTTv5)
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.connect(mqhost, mqport, 60)
    mqttc.loop_start()


def messagesview(page: ft.Page):
    global messagefield

    def sendmessageclicked(e):
        sendmessage(messagefield.value)

    messagefield = ft.TextField(label="message", expand=True)
    print(divider)
    print("displaying messages for:", currentcontact)
    cont = dbwrap.getcontactbypublictransport(currentcontact)
    print("contact:", cont)
    messages = []
    for i in getconversation(currentcontact):
        print("got message:", i)
        if i.type == 1:
            messages.append(
                ft.Container(
                    ft.ListTile(
                        title=ft.Text(i.payload.decode("UTF-8")),
                    ),
                    border_radius=10,
                    bgcolor="#0000ff" if i.source == currentcontact else "#00ffaa",
                )
            )
    outp = ft.Column()
    outp.controls.append(
        ft.AppBar(
            title=ft.Text(cont.username),
            center_title=True,
            bgcolor=ft.colors.SURFACE_VARIANT,
        )
    )
    # for i in messages:
    #     outp.controls.append(i)
    outp.controls.append(ft.ListView(controls=messages, expand=True))
    outp.controls.append(
        ft.Row(
            controls=[
                messagefield,
                ft.IconButton(
                    icon=ft.icons.PAUSE_CIRCLE_FILLED_ROUNDED,
                    icon_size=20,
                    tooltip="send message",
                    on_click=sendmessageclicked,
                ),
            ]
        )
    )
    return outp
    # print('done adding messages')
    # conts = [
    #     [ft.AppBar(
    #         title=ft.Text(cont.username),
    #         center_title=True,
    #         bgcolor=ft.colors.SURFACE_VARIANT)] +
    #         # leading=ft.Image(src=iconsfile + hashtoiconname(currentcontact) + '.png'))] +
    #     [ft.ListView(controls=messages)] +
    #     [ft.Row(controls=[
    #         messagefield,
    #         ft.IconButton(
    #             icon='assets/icon.png',
    #             icon_size=20,
    #             tooltip='send message',
    #             on_click=lambda e: sendmessage(messagefield.value)
    #             )
    #         ]
    #     )]
    # ]
    # for i in conts:
    #     page.add(i)
    # print('calling update')
    # page.update()
    # page.views.append(
    #     ft.View(
    #         '/messages',
    #         [ft.AppBar(
    #             title=ft.Text(cont.username),
    #             center_title=True,
    #             bgcolor=ft.colors.SURFACE_VARIANT)] +
    #             # leading=ft.Image(src=iconsfile + hashtoiconname(currentcontact) + '.png'))] +
    #         [ft.ListView(controls=messages)] +
    #         [ft.Row(controls=[
    #             messagefield,
    #             ft.IconButton(
    #                 icon='assets/icon.png',
    #                 icon_size=20,
    #                 tooltip='send message',
    #                 on_click=lambda e: sendmessage(messagefield.value)
    #                 )
    #             ]
    #         )]
    #     )
    # )
    # print('done')


def contactsview(page: ft.Page):

    def messageview(e: ft.RouteChangeEvent):
        global currentcontact
        currentcontact = e.control.data
        print(e.control.data)
        page.go("/messages")

    conts = getcontacts()
    contactlist = []
    for i in conts:
        # print('path:',iconsfile + hashtoiconname(i.pubkey_transport))
        contactlist.append(
            ft.ListTile(
                title=ft.Text(i.username),
                leading=ft.Image(
                    src=iconsfile + hashtoiconname(i.pubkey_transport) + ".png",
                    fit="contain",
                ),
                # leading=ft.Image(src='assets/icon.png', fit='contain'),
                on_click=messageview,
                data=i.pubkey_transport,
            )
        )
    # page.views.append(
    #     ft.View(
    #         "/",
    #         [ft.AppBar(title=ft.Text("JANNIE GASSER (BASED)"), bgcolor=ft.colors.SURFACE_VARIANT)]
    #         # contactlist
    #     ))
    # page.views.append(ft.AppBar(title=ft.Text("JANNIE GASSER (BASED)"), bgcolor=ft.colors.SURFACE_VARIANT))
    # page.views.append(ft.ListView(controls=contactlist))
    outp = ft.Column()
    # outp.controls.append(ft.Text('hi'))
    outp.controls.append(
        ft.AppBar(
            title=ft.Text("JANNIE GASSER (BASED)"), bgcolor=ft.colors.SURFACE_VARIANT
        )
    )
    for i in contactlist:
        outp.controls.append(i)
    return outp


def main(page: ft.Page):
    global currentcontact
    global messagefield
    # os.environ['DATABASE_URL'] = 'postgresql://localhost:pass123@database:17891/postgres'
    # print('env var:', os.environ['DATABASE_URL'])
    # print(dbwrap.getcontacts())
    # print(dbwrap.getcontactbypublictransport(dbwrap.getmessages("TsXJOnR6CzYqoOQysflqwOkk4M2newKV3SoDcS8I9yQ=")))
    # page.add(ft.SafeArea(ft.Text("Hello, Flet!")))
    lv = ft.ListView(expand=1, spacing=10, padding=20, auto_scroll=True)
    ft.TextField(label="messageinput")

    page.add(ft.Text(f"Initial route: {page.route}"))

    def route_change(route):
        elems = None
        page.views.clear()
        if page.route == "/":
            elems = contactsview(page)
        if page.route == "/messages":
            elems = messagesview(page)

        page.views.append(elems)
        page.update()

    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    # def messageview(e: ft.RouteChangeEvent):

    # print(conts)
    # exit(0)

    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)
    # for i in conts:
    #     lv.controls.append()
    # page.add(lv)
    # ft.app(target=main)


def testdb():
    dbwrap = dbwrapper()
    print(dbwrap.getmessages("TsXJOnR6CzYqoOQysflqwOkk4M2newKV3SoDcS8I9yQ="))
    print("done")


def testmqtt():
    global mqttc
    testchannel = "testchannel"

    print("publishing message")
    mqttc.subscribe(testchannel)
    mqttc.subscribe(clientid)
    mqttc.publish(testchannel, "testing")
    executecommand(mqttc, testchannel, "testcommand")
    time.sleep(1)


# testdb()
if __name__ == "__main__":
    init()
    # ft.app(main)
    testmqtt()
